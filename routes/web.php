<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\CashAdvancedsController;
use App\Http\Controllers\PettyCashesController;
use App\Http\Controllers\ReimburseController;
use App\Http\Controllers\PayReqsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\RejectionController;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', [App\Http\Controllers\CashAdvancedsController::class, 'index'])->name('home');

//Finance Report
Route::get('/finreport',[App\Http\Controllers\FinanceController::class, 'index'])->name('financereport');
Route::get('/finreport/status/close/{id}',[App\Http\Controllers\FinanceController::class, 'close_report'])->name('financereport_close');

//Approval Dates
Route::get('/approval/ca',[App\Http\Controllers\ApprovalController::class, 'index_ca'])->name('approval_ca');
Route::get('/approval/pc',[App\Http\Controllers\ApprovalController::class, 'index_pc'])->name('approval_pc');
Route::get('/approval/rm',[App\Http\Controllers\ApprovalController::class, 'index_rm'])->name('approval_rm');

//Rejection
Route::get('/rejections/{type}',[App\Http\Controllers\RejectionController::class, 'index'])->name('show_rejections');

//CA
Route::resource('careport', CashAdvancedsController::class);
Route::get('/careportall', [App\Http\Controllers\CashAdvancedsController::class,'indexall']);
Route::post('/careport/update/{id}', [App\Http\Controllers\CashAdvancedsController::class,'update']);
Route::get('/careport/get/{id}', [App\Http\Controllers\CashAdvancedsController::class,'getCaFile']);
Route::get('/careport/approve-0/{id}', [App\Http\Controllers\CashAdvancedsController::class,'approve_0']);
Route::get('/careport/approve-2/{id}', [App\Http\Controllers\CashAdvancedsController::class,'approve_2']);
Route::get('/careport/approve-3/{id}', [App\Http\Controllers\CashAdvancedsController::class,'approve_3']);
Route::get('/careport/approve-4/{id}', [App\Http\Controllers\CashAdvancedsController::class,'approve_4']);
Route::get('/careport/approve-5/{id}', [App\Http\Controllers\CashAdvancedsController::class,'approve_5']);
Route::get('/careport/approve-6/{id}', [App\Http\Controllers\CashAdvancedsController::class,'approve_6']);
Route::get('/careport/approve-7/{id}', [App\Http\Controllers\CashAdvancedsController::class,'approve_7']);
Route::get('/careport/approve-8/{id}', [App\Http\Controllers\CashAdvancedsController::class,'approve_8']);
Route::get('/careport/reject/{id}', [App\Http\Controllers\CashAdvancedsController::class,'reject']);
Route::get('/careport/multi/ca/allow/{id}', [App\Http\Controllers\CashAdvancedsController::class,'allow_multiple_ca']);
Route::get('/careport/multi/ca/revoke/{id}', [App\Http\Controllers\CashAdvancedsController::class,'revoke_multiple_ca']);

//PC
Route::resource('pcreport', PettyCashesController::class);
Route::get('/pcreportall', [App\Http\Controllers\PettyCashesController::class,'indexall']);
Route::get('/pcreport/approve-1/{id}', [App\Http\Controllers\PettyCashesController::class,'approve_1']);
Route::get('/pcreport/approve-2/{id}', [App\Http\Controllers\PettyCashesController::class,'approve_2']);
Route::get('/pcreport/approve-3/{id}', [App\Http\Controllers\PettyCashesController::class,'approve_3']);
Route::get('/pcreport/approve-4/{id}', [App\Http\Controllers\PettyCashesController::class,'approve_4']);
Route::get('/pcreport/approve-5/{id}', [App\Http\Controllers\PettyCashesController::class,'approve_5']);
Route::get('/pcreport/approve-6/{id}', [App\Http\Controllers\PettyCashesController::class,'approve_6']);
Route::get('/pcreport/approve-7/{id}', [App\Http\Controllers\PettyCashesController::class,'approve_7']);
Route::get('/pcreport/reject/{id}', [App\Http\Controllers\PettyCashesController::class,'reject']);

//Payment Request
Route::resource('payreqreport', PayReqsController::class);
Route::post('/payreqreport/update/{id}', [App\Http\Controllers\PayReqsController::class,'update']);
Route::get('/payreqreport/get/{id}', [App\Http\Controllers\PayReqsController::class,'getCaFile']);
Route::get('/payreqreport/approve-0/{id}', [App\Http\Controllers\PayReqsController::class,'approve_0']);
Route::get('/payreqreport/approve-2/{id}', [App\Http\Controllers\PayReqsController::class,'approve_2']);
Route::get('/payreqreport/approve-3/{id}', [App\Http\Controllers\PayReqsController::class,'approve_3']);
Route::get('/payreqreport/approve-4/{id}', [App\Http\Controllers\PayReqsController::class,'approve_4']);
Route::get('/payreqreport/approve-5/{id}', [App\Http\Controllers\PayReqsController::class,'approve_5']);
Route::get('/payreqreport/approve-6/{id}', [App\Http\Controllers\PayReqsController::class,'approve_6']);
Route::get('/payreqreport/approve-7/{id}', [App\Http\Controllers\PayReqsController::class,'approve_7']);
Route::get('/payreqreport/approve-8/{id}', [App\Http\Controllers\PayReqsController::class,'approve_8']);
Route::get('/payreqreport/reject/{id}', [App\Http\Controllers\PayReqsController::class,'reject']);

//Reimburse
// Route::post('/reimbursereport/update/{id}', [App\Http\Controllers\ReimburseController::class,'update']);

Route::resource('reimbursereport', ReimburseController::class)->except('update');
Route::group(['prefix' => 'reimbursereport', 'namespace' => 'reimbursereport'], function ()
{
        Route::post('/update/{id}', [App\Http\Controllers\ReimburseController::class,'update']);
        Route::get('/delete/{id}', [App\Http\Controllers\ReimburseController::class,'destroy']);
        Route::get('/approve-0/{id}', [App\Http\Controllers\ReimburseController::class,'approve_0']);
        Route::get('/approve-2/{id}', [App\Http\Controllers\ReimburseController::class,'approve_2']);
        Route::get('/approve-3/{id}', [App\Http\Controllers\ReimburseController::class,'approve_3']);
        Route::get('/approve-4/{id}', [App\Http\Controllers\ReimburseController::class,'approve_4']);
        Route::get('/approve-5/{id}', [App\Http\Controllers\ReimburseController::class,'approve_5']);
        Route::get('/approve-6/{id}', [App\Http\Controllers\ReimburseController::class,'approve_6']);
        Route::get('/approve-7/{id}', [App\Http\Controllers\ReimburseController::class,'approve_7']);
        Route::get('/approve-8/{id}', [App\Http\Controllers\ReimburseController::class,'approve_8']);
        Route::get('/reject/{id}', [App\Http\Controllers\ReimburseController::class,'reject']);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);

// USER
Route::resource('user', UsersController::class)->except('delete','show');
Route::group(['prefix' => 'user', 'namespace' => 'reimbursereport'], function ()
{
        Route::post('/update/{id}', [App\Http\Controllers\UsersController::class,'update']);
        Route::post('/create', [App\Http\Controllers\UsersController::class,'create']);
        Route::post('/new', [App\Http\Controllers\UsersController::class,'store']);
        Route::get('/delete/{id}', [App\Http\Controllers\UsersController::class,'destroy']);
        Route::get('/profile', [App\Http\Controllers\UsersController::class,'show_profile'])->name('user.profile');
        Route::post('/update/password/{id}', [App\Http\Controllers\UsersController::class,'update_password']);
});
