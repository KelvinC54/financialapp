<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CashAdvancedsController;
use App\Http\Controllers\PettyCashesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/careport/upload', [App\Http\Controllers\CashAdvancedsController::class,'upload_ca_file_ajax']);
Route::post('/pcreport/upload', [App\Http\Controllers\PettyCashesController::class,'approve_1']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
