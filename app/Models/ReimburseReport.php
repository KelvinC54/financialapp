<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReimburseReport extends Model
{
    use HasFactory;

    protected $table = "reimburse_reports";

    protected $fillable = [
        //Meta Data
        'project_code',
        'project_name',
        'week_reimbursement',
        'user_id',
        'user_name',
        'user_designation',
        'total_reimbursement',
        'user_remarks',
        'remark_reimbursement',
        'status_reimbursement',

        //Files
        'file_reimbursement',
        'url_reimbursement',
    ];
}
