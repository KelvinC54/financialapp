<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentRequest extends Model
{
    protected $table = "payreq_reports";

    protected $fillable = [
        //Meta Data
        'user_id',
        'user_name',
        'user_designation',
        'payreq_project_code',
        'payreq_project_name',
        'payreq_week',

        //payreq
        'payreq_type',
        'payreq_total',
        'user_remarks',
        'payreq_status',
        'payreq_remarks',

        //Files
        'payreq_file', 
        'payreq_url'                   

    ];

    protected $primaryKey = 'payreq_id';
    
    public $timestamps = false;
}
