<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = "finance_reports";

    // protected $fillable = [
    //     //Meta Data
    //     'user_id',
    //     'user_name',
    //     'user_designation',
    //     'finance_project_code',
    //     'finance_project_name',
    //     'finance_week',
    //     'transfer_remarks',

    //     //CA
    //     'ca_type',
    //     'ca_total',
    //     'user_remarks',
    //     'ca_transfered',
    //     'ca_aging',
    //     'ca_status',

    //     //PC
    //     'pc_total',
    //     'user_remarks_pc',
    //     'pc_receipt_back_from_ca',
    //     'pc_date_creation',
    //     'pc_status',

    //     //Files
    //     'ca_file',
    //     'ca_url',
    //     'pc_file',
    //     'pc_url',

    // ];

    protected $primaryKey = 'finance_id';

    public $timestamps = false;

}
