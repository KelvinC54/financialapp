<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RejectionRecord extends Model
{
    use HasFactory;

    protected $table = "rejection_records";

    protected $guarded = [];
}
