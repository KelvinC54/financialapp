<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApprovalRecord extends Model
{
    use HasFactory;

    protected $table = "approval_records";

    protected $fillable = [
        'finance_id',
        'user_id',
        'finance_type',
        'date',
    ];
}
