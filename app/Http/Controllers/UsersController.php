<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Storage;

class UsersController extends Controller
{
	
	public function index()
	{
		$users = DB::table('users')
			->where('designation','!=','webadmin')
			->get();
		return view('pages.users.show', ['users' => $users]);
	}

	public function create()
	{
		$jobdesk = DB::table('jobdesk')
			->select('*')
			->orderBy('kode_jobdesk', 'ASC')
			->get();

		return view('pages.users.create',['jobdesk'=>$jobdesk]);
	}

	// Upload + Validasi Gambar Sign
	// private function uploadSign(Request $request) {
	// 	$gambar = $request->file('sign');
	// 	$type = $gambar->getClientOriginalExtension();

	// 	if($request->file('sign')->isValid()) {
	// 		$filename = "Sign-" . $request->email . "." . $type;
	// 		$uploadPath = "images/sign/";
	// 		$request->file('sign')->move($uploadPath, $filename);
	// 		return $filename;
	// 	}
	// 	return false;
	// }

	// // Hapus Gambar Sign di Local Drive
	// private function hapusSign(User $user) {
	// 	$exist = Storage::disk('sign')->exist($user->sign);
	// 	if(isset($user->sign) && $exist) {
	// 		$delete = Storage::disk('sign')->delete($user->sign);
	// 		if($delete) {
	// 			return true;
	// 		}
	// 		return false;
	// 	}
	// }

	// // Delete Gambar Sign
	// private function deleteSign($request) {
	// 	$file = 'images/sign/'.$request->sign;
	// 	if(is_file($file)) {
	// 		unlink($file);
	// 	}
	// }

	public function store(Request $request)
	{
		$jobdesk = DB::table('jobdesk')
			->select('nama_jobdesk')
			->where('kode_jobdesk', $request->kode_jobdesk)
			->first();

		$user = new User;
		$user->nip = $request->nip;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->real_password = $request->password;
		$user->password = Hash::make($request->password);
		$user->jobdesk = $request->kode_jobdesk;
        $user->designation = $jobdesk->nama_jobdesk;
		$user->save();

		return redirect()->back()->with('create_success', 'User Berhasil ditambahkan!');
	}

	public function show_profile()
	{
		$jobdesk = DB::table('jobdesk')
			->select('*')
			->orderBy('kode_jobdesk', 'ASC')
			->get();

		return view('pages.users.profile', ['jobdesk'=>$jobdesk]);
	}

	public function edit($id)
	{
		$users = User::findOrFail($id);

		$jobdesk = DB::table('jobdesk')
			->select('*')
			->orderBy('kode_jobdesk', 'ASC')
			->get();

		// dd($jobdesk);

		return view('pages.users.edit', ['user' => $users, 'jobdesk' => $jobdesk]);
	}

	public function update(Request $request, $id)
	{
		$jobdesk = DB::table('jobdesk')
			->select('nama_jobdesk')
			->where('kode_jobdesk', $request->kode_jobdesk)
			->first();

		$user = User::findOrFail($id);
		$user->nip = $request->nip;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->real_password = $request->password;
		$user->password = Hash::make($request->password);
		$user->jobdesk = $request->kode_jobdesk;
        $user->designation = $jobdesk->nama_jobdesk;
		$user->save();

		return redirect()->back()->with('update_success', 'Data Berhasil di Update!');
	}

	public function update_password(Request $request, $id)
	{
		$messages = [
            'required' => 'kolom :attribute harus diisi',
        ];

        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|confirmed',
        ], $messages);

        if ($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
        } else {
			$user = User::findOrFail($id);
			$user->real_password = $request->password;
			$user->password = Hash::make($request->password);
			$user->save();
	
			return redirect()->back()->with('update_password_success', 'Password Berhasil diubah!');
        }

	}

	public function destroy($id)
	{
		$users = User::findOrFail($id);
		$users->delete();

		return redirect()->back()->with('delete_success', 'User Berhasil dihapus!');
	}
}
