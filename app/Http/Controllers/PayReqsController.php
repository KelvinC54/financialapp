<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\PaymentRequest;
use App\Models\ApprovalRecord;
use App\Models\Project;
use App\Models\RejectionRecord;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;
use Storage;

class PayReqsController extends Controller
{

    public function index()
	{
		$reports = DB::table('payreq_reports')->get();
        $current_date_time = \Carbon\Carbon::now();

		return view('pages.payreqreport', ['reports' => $reports, 'current_date_time' => $current_date_time]);
	}

    public function create()
	{
        $projects = collect(Project::select('*')->get())->keyBy('project_code');
        
        $user_unclosed_projects = DB::table('payreq_reports')->get();
        // $user_unclosed_projects = DB::table('payreq_reports')
        //     ->where('user_id', Auth::user()->id)
        //     ->where('closed_status', '!=', 1)
        //     ->select('payreq_project_code')
        //     ->get();

        // $user_unclosed_projects = DB::table('payreq_reports')
        //     ->where('user_id', Auth::user()->id)
        //     ->where('closed_status', '!=', 1)
        //     ->select('payreq_project_code', 'closed_status', 'multiple_ca_on_project')
        //     ->get();

        foreach ($user_unclosed_projects as $u) {
            $projects[$u->payreq_project_code];

            // if ($u->closed_status == 1) {
            //     continue;
            // } elseif ($u->closed_status == 0 && $u->multiple_ca_on_project == 1) {
            //     continue;
            // } else {
            //     unset($projects[$u->payreq_project_code]);
            // }

        }

        return view('pages.createpayreq',['projects'=>$projects]);
	}

    private function deletePayReqFile($request) {
		$file = 'payreqfile/'.$request->cafile;
		if(is_file($file)) {
			unlink($file);
		}
	}

    // private function getCaFile($id){
    //     $file = "cafile/";
    //     return response()->download();

    // }

    private function upload_payreq_file($request) {
        $file = $request->payreqfile;
        $filename = time() . "-payreqfile-" . $request->payreq_project_code . "-" . $file->getClientOriginalName();
        $uploadPath = "payreqfile/";
        $request->file('payreqfile')->move($uploadPath, $filename);
        return $filename;
    }

    public function upload_payreq_file_ajax(Request $request) {
        $file = $request->cafile;
        $filename = time() . "-payreqfile-" . $request->payreq_project_code . "-" . $file->getClientOriginalName();
        $uploadPath = "payreqfile/";
        $request->file('payreqfile')->move($uploadPath, $filename);
        return response()->json(['success' => true,'nama_file'=>$filename]);
    }

    public function store(Request $request)
    {
        $messages = [
            'required' => 'kolom :attribute harus diisi',
        ];

        $validator = Validator::make($request->all(), [
            'payreq_project_code' => 'required',
            'payreq_project_name' => 'required',
            'payreq_total' => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return redirect('/payreqreport/create')->withErrors($validator)->withInput();
        } else {
            $user_id = Auth::id();
            $current_date_time = \Carbon\Carbon::now();

            if($request->hasFile('payreqfile')) {
                DB::table('payreq_reports')->insert([
                    // Meta Data
                    'user_id' => $user_id,
                    'user_name'=> $request->user()->name,
                    'user_designation'=> $request->user()->designation,
                    'payreq_project_code' => $request->payreq_project_code,
                    'payreq_project_name'=> $request->payreq_project_name,
                    'payreq_week'=> $request->payreq_week,
                    'updated_at' => $current_date_time,
                    'created_at' => $current_date_time,

                    //PayReq
                    'payreq_type' =>$request->payreq_type,
                    'user_remarks' =>$request->user_remarks,
                    'payreq_total' => $request->payreq_total,
                    'payreq_status' => 0,

                    //File PayReq
                    'payreq_file' => $this->upload_payreq_file($request)
                ]);
            }
            else {
                DB::table('payreq_reports')->insert([
                    // Meta Data
                    'user_id' => $user_id,
                    'user_name'=> $request->user()->name,
                    'user_designation'=> $request->user()->designation,
                    'payreq_project_code' => $request->payreq_project_code,
                    'payreq_project_name'=> $request->payreq_project_name,
                    'payreq_week'=> $request->payreq_week,
                    'updated_at' => $current_date_time,
                    'created_at' => $current_date_time,

                    //PayReq
                    'payreq_type' =>$request->payreq_type,
                    'user_remarks' =>$request->user_remarks,
                    'payreq_total' => $request->payreq_total,
                    'payreq_status' => 0,

                    //File PayReq
                    'payreq_url' => $request->payreq_url
                ]);
            }
            return redirect('payreqreport')->with('success','Post created successfully.');
        }
    }

    //Approval PM Reg
    public function approve_0($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('payreq_reports')
			->where('payreq_id', $id)
			->update([
			    'payreq_status' => 2,
                'updated_at' => $current_date_time,
			    ]);

        // $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/payreqreport')->with('message', 'Payment Request Berhasil Di Approve!');
	}

    //Approval PM Nat
	public function approve_2($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
        $document = DB::table('payreq_reports')
			->where('payreq_id', $id)
			->update([
			    'payreq_status' => 3,
                'updated_at' => $current_date_time,
			    ]);

        // $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/payreqreport')->with('message', 'Payment Request Berhasil Di Approve!');
	}

    //Approval RCM
	public function approve_3(Request $request, $id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('payreq_reports')
			->where('payreq_id', $id)
			->update([
			    'payreq_status' => 4,
                'updated_at' => $current_date_time
			    ]);

        // $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/payreqreport')->with('message', 'Payment Request Berhasil Di Approve!');
	}

    //Approval DH
	public function approve_4($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('payreq_reports')
			->where('payreq_id', $id)
			->update([
			    'payreq_status' => 5,
                'updated_at' => $current_date_time,
			]);

        // $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/payreqreport')->with('message', 'Payment Request Berhasil Di Approve!');
	}

    //Approval Akuntan Ap
	public function approve_5($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('payreq_reports')
			->where('payreq_id', $id)
			->update([
			    'payreq_status' => 6,
                'updated_at' => $current_date_time,
			    ]);

        // $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/payreqreport')->with('message', 'Payment Request Berhasil Di Approve!');
	}

    //Approval Akuntan Head
	public function approve_6(Request $request, $id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $document = DB::table('payreq_reports')
            ->where('payreq_id', $id)
            ->select(['payreq_total'])
            ->first();

        if ($document->ca_total >= 10000000) {
            $payreq_status = 7;
        } else {
            $payreq_status = 8;
        }

        $update_document = DB::table('payreq_reports')
            ->where('payreq_id', $id)
            ->update([
                'payreq_status' => $payreq_status,
                'updated_at' => $current_date_time
                ]);

        // $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/payreqreport')->with('message', 'Payment Request Berhasil Di Approve!');
	}

    //Approval BOD
    public function approve_7(Request $request, $id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('payreq_reports')
			->where('payreq_id', $id)
			->update([
			    'payreq_status' => 8,
                'updated_at' => $current_date_time
			    ]);

        // $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/payreqreport')->with('message', 'Payment Request Berhasil Di Approve!');
	}

    //Approval Akuntan Tf
    public function approve_8($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('payreq_reports')
			->where('payreq_id', $id)
			->update([
			    'payreq_status' => 9,
                'updated_at' => $current_date_time
			    ]);

        // $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/payreqreport')->with('message', 'Payment Request Berhasil Di Approve!');
	}

    public function reject(Request $request, $id)
	{
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time_string = $current_date_time->toDayDateTimeString();

        $report = PaymentRequest::findOrFail($id);
        $report->payreq_status = 1;
        $report->updated_at = $current_date_time;
        $report->payreq_remarks = $request->doc_remarks;
        $report->save();

        $rejection_record = RejectionRecord::create([
            'user_id' => Auth::user()->id,
            'user_name' => Auth::user()->name,
            'remarks' => $request->doc_remarks,
            'finance_id' => $id,
            'project_code' => $report->payreq_project_code,
            'project_name' => $report->payreq_project_name,
            'finance_type' => 'PR',
            'finance_week' => $report->payreq_week,
            'submitted_by_id' => $report->user_id,
            'submitted_by_name' => $report->user_name,
            'total' => $report->payreq_total,
            'date' => $current_date_time_string,
        ]);

		return redirect('/payreqreport')->with('reject_success', 'Payment Request Berhasil Di Reject!');
	}   

    public function edit($id)
    {
        $payreq_reports = Report::findOrFail($id);
		return view('pages.editpayreq', ['payreq_reports' => $payreq_reports]);
    }

    public function update(Request $request, $id)
    {
        $payreq_reports = Report::findOrFail($id);

        if($request->hasFile('ca_file')) {
            $this->deleteCaFile($payreq_reports);
			$payreq_reports->ca_file = $this->upload_ca_file($request);
        }

        $payreq_reports->save();

        DB::table('payreq_reports')->where('payreq_id',$id)->update([
			// Meta Data
            'payreq_project_code' => $request->payreq_project_code,
            'payreq_project_name'=> $request->payreq_project_name,
            'payreq_week'=> $request->payreq_week,

            //CA
            'ca_type' =>$request->ca_type,
            'user_remarks' =>$request->doc_remarks,
            'ca_total' => $request->ca_total,
            'payreq_status' => 0,

			//File CA
            'ca_url' => $request->ca_url

		]);

        return redirect()->back()->with('update_success', 'Payment Request Berhasil diupdate!');

    }

    public function destroy($id)
    {
        $payreq_reports = Report::findOrFail($id);
        $this->deleteCaFile($payreq_reports);
        $payreq_reports->delete();

        return redirect('/payreqreport')->with('message', 'Payment Request Berhasil Dihapus!');
    }

    protected function add_ca_approval_record($payreq_id, $current_date_time)
    {
        $id_user = Auth::user()->id;

        $approval_record = new ApprovalRecord;

        $approval_record->payreq_id = $payreq_id;
        $approval_record->user_id = $id_user;
        $approval_record->payreq_type = 'CA';
        $approval_record->date = $current_date_time;
        $approval_record->save();
    }

    public function allow_multiple_ca($id)
	{
        $report = Report::findOrFail($id);
        $report->multiple_ca_on_project = 1;
        $report->save();

		return redirect('/payreqreport')->with('allow_success', 'Allow multiple CA sukses!');
	}

    public function revoke_multiple_ca($id)
	{
        $report = Report::findOrFail($id);
        $report->multiple_ca_on_project = 0;
        $report->save();

		return redirect('/payreqreport')->with('revoke_success', 'Revoke izin multiple CA sukses!');
	}
}
