<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_ca()
    {
        $projects = collect( DB::table('approval_records')
            ->Join('users', 'approval_records.user_id', '=', 'users.id')
            ->Join('finance_reports', 'approval_records.finance_id', '=', 'finance_reports.finance_id')
            ->where('approval_records.finance_type', 'CA')
            ->select(
                'finance_reports.finance_project_code',
                'finance_reports.finance_project_name', 
                )
            ->get()
            )->keyBy('finance_project_code');
        
        $reports = $this->get_approval('CA', 'ca_total');

        // dd($reports);

		return view('pages.approval_ca', [
            'projects' => $projects,
            'reports' => $reports,
        ]);
    }

    public function index_pc()
    {
        $projects = collect( DB::table('approval_records')
        ->Join('users', 'approval_records.user_id', '=', 'users.id')
        ->Join('finance_reports', 'approval_records.finance_id', '=', 'finance_reports.finance_id')
        ->where('approval_records.finance_type', 'PC')
        ->select(
            'finance_reports.finance_project_code',
            'finance_reports.finance_project_name', 
            )
        ->get()
        )->keyBy('finance_project_code');

        $reports = $this->get_approval('PC', 'pc_total');

		return view('pages.approval_pc', [
            'projects' => $projects,
            'reports' => $reports
        ]);
    }

    public function index_rm()
    {
        $projects = collect( DB::table('approval_records')
        ->Join('users', 'approval_records.user_id', '=', 'users.id')
        ->Join('reimburse_reports', 'approval_records.finance_id', '=', 'reimburse_reports.id')
        ->where('approval_records.finance_type', 'RM')
        ->select(
            'reimburse_reports.project_code',
            'reimburse_reports.project_name', 
            )
        ->get()
        )->keyBy('project_code');

        $reports = $this->get_approval('RM', 'total_reimbursement');

		return view('pages.approval_rm', [    
            'projects' => $projects,
            'reports' => $reports
        ]);
    }

    protected function get_approval($finance_type, $total_type)
    {
        if ($finance_type == 'RM') {
            $approval = DB::table('approval_records')
                ->Join('users', 'approval_records.user_id', '=', 'users.id')
                ->Join('reimburse_reports', 'approval_records.finance_id', '=', 'reimburse_reports.id')
                ->where('approval_records.finance_type', 'RM')
                ->select(
                    'reimburse_reports.project_code',
                    'reimburse_reports.project_name', 
                    'reimburse_reports.week_reimbursement', 
                    'reimburse_reports.user_name', 
                    'reimburse_reports.total_reimbursement', 
                    'users.name', 
                    'approval_records.date',
                    )
                ->get();
        } else {
            $approval = DB::table('approval_records')
            ->Join('users', 'approval_records.user_id', '=', 'users.id')
            ->Join('finance_reports', 'approval_records.finance_id', '=', 'finance_reports.finance_id')
            ->where('approval_records.finance_type', $finance_type)
            ->select(
                'finance_reports.finance_project_code',
                'finance_reports.finance_project_name', 
                'finance_reports.finance_week', 
                'finance_reports.user_name', 
                'finance_reports.'. $total_type , 
                'users.name', 
                'approval_records.date',
                )
            ->get();
        }

        return $approval;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
