<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Report;
use App\Models\ApprovalRecord;
use App\Models\RejectionRecord;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Storage;

class PettyCashesController extends Controller
{
    
    public function indexall()
    {
        $reports = Report::orderBy('finance_id', 'DESC')->get();
        // $reports = DB::table('finance_reports')->get();
		return view('pages.pcreportall', ['reports' => $reports]);
    }
    
    public function index()
    {
        $reports = Report::orderBy('finance_id', 'DESC')->get();
        // $reports = DB::table('finance_reports')->get();
		return view('pages.pcreport', ['reports' => $reports]);
    }
    
    public function create()
    {
        return view('pages.createpc');
    }

    public function store(Request $request)
    {
        $messages = [
            'required' => 'kolom :attribute harus diisi',
        ];

        $validator = Validator::make($request->all(), [
            'pc_total' => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return redirect('/pcreport/create')->withErrors($validator)->withInput();
        } else {
            $finance_reports = Report::findOrFail($request->project_id);
            
            if(($request->user()->skip_approval == 1) && ($request->user()->user_location == "1") ){
                $finance_reports->pc_status = 5;
            }
            elseif(($request->user()->skip_approval == 1) && ($request->user()->user_location == "2") ){
                $finance_reports->pc_status = 5;
            }
            elseif($request->user()->skip_approval == 2){
                $finance_reports->pc_status = 2;
            }
            elseif($request->user()->skip_approval == 3){
                $finance_reports->pc_status = 3;
            }
            else {
                $finance_reports->pc_status = 1;
            }

        if($request->hasFile('pcfile')) {
            $this->deletePcFile($finance_reports);
			$finance_reports->pc_file = $this->upload_pc_file($request, $request->project_code);
        }

        if($request->hasFile('pc_receipt_back_from_ca')) {
            // $this->deleteReceiptFile($finance_reports);
			$finance_reports->pc_receipt_back_from_ca = $this->upload_receipt_file($request, $request->project_code);
        } else{
            $finance_reports->pc_receipt_back_from_ca = $request->pc_receipt_back_from_ca;
        }

        $finance_reports->ca_status = 10;
        $finance_reports->pc_total = $request->pc_total;
        
        
        $finance_reports->pc_url = $request->pc_url;
        $finance_reports->user_remarks_pc = $request->doc_remarks;

        $finance_reports->save();

        $reports = DB::table('finance_reports')->get();
        return view('pages.pcreport', ['reports' => $reports]);
        }
    }

    private function upload_pc_file($request, $project_code) {

        $arr_filename = [];

        foreach($request->file('pcfile') as $file)
        {
            $file_type = $file->guessExtension();
            if($file->isValid()) {
                $filename = time() . "-pcfile-" . $project_code . $file->getClientOriginalName();
                $uploadPath = "pcfile/";
                $file->move($uploadPath, $filename);
                array_push($arr_filename, $filename);
            }
        }

        return implode(",",$arr_filename);
    }

    private function upload_receipt_file($request, $project_code) {
        $arr_filename = [];

        foreach($request->file('pc_receipt_back_from_ca') as $file)
        {
            $file_type = $file->guessExtension();
            if($file->isValid()) {
                $filename = time() . "-pc_receipt_back_from_ca-" . $project_code . $file->getClientOriginalName();
                $uploadPath = "pc_receipt_back_from_ca/";
                $file->move($uploadPath, $filename);

                array_push($arr_filename, $filename);
            }
        }

        return implode(",",$arr_filename);
    }


    private function deletePcFile($request) {
		// $file = 'pcfile/'.$request->cafile;
		$file = 'pc_receipt_back_from_ca/'.$request->file;
		if(is_file($file)) {
			unlink($file);
		}
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $reports = DB::table('finance_reports')->get();
        // return view('pages.createpc',['reports'=>$reports]);

        $finance_reports = Report::findOrFail($id);
        return view('pages.createpc',['report'=>$finance_reports]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $finance_reports = Report::findOrFail($id);
        
        if(($request->user()->skip_approval == 1) && ($request->user()->user_location == "1") ){
            $pc_status = 5;
        }
        elseif(($request->user()->skip_approval == 1) && ($request->user()->user_location == "2") ){
            $pc_status = 4;
        }
        else {
            $pc_status = 1;
        }

        if($request->hasFile('pcfile')) {
            $this->deletePcFile($finance_reports);
			$finance_reports->pc_file = $this->upload_pc_file($request, $request->project_code);
        }

        if($request->hasFile('pc_receipt_back_from_ca')) {
            $this->deleteReceiptFile($finance_reports);
			$finance_reports->pc_receipt_back_from_ca = $this->upload_receipt_file($request, $request->project_code);
        }

        $finance_reports->save();

        DB::table('finance_reports')->where('finance_id',$id)->update([

			// Meta Data
            'finance_project_code' => $request->finance_project_code,
            'finance_project_name'=> $request->finance_project_name,
            'finance_week'=> $request->finance_week,
            'ca_status' => 10,
            'pc_total'=> $request->pc_total,
            'pc_status' => $pc_status,
            'user_remarks_pc' => $request->doc_remarks,

			//File PC
            'pc_url' => $request->pc_url

		]);

        return redirect()->back();
    }
    
    // Approval PMR
    public function approve_1($id)
	{
	    $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'pc_status' => 2,
			    'updated_at' => $current_date_time,
			    ]);

        $this->add_pc_approval_record($id, $current_date_time_string);

		return redirect('/pcreport')->with('message', 'Dokumen Berhasil Di Approve!');
	}
	
	// Approval PMN
	public function approve_2($id)
	{
	    $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
        $document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
                'pc_status' => 3,
			    'updated_at' => $current_date_time,
                ]);

        $this->add_pc_approval_record($id, $current_date_time_string);

		return redirect('/pcreport')->with('message', 'Dokumen Berhasil Di Approve!');
	}
	
	// Approval RCM
	public function approve_3($id)
	{
	    $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'pc_status' => 5,
			 //   Skip Approval DH
			 //   'pc_status' => 4,
			    'updated_at' => $current_date_time,
			    ]);

        $this->add_pc_approval_record($id, $current_date_time_string);

		return redirect('/pcreport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_4($id)
	{
	    $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'pc_status' => 5,
			    'updated_at' => $current_date_time,
			]);

        $this->add_pc_approval_record($id, $current_date_time_string);

		return redirect('/pcreport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_5($id)
	{
	    $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'pc_status' => 6,
			    'updated_at' => $current_date_time,
			    ]);

        $this->add_pc_approval_record($id, $current_date_time_string);

		return redirect('/pcreport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_6($id)
	{
	    $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'pc_status' => 7,
			    'updated_at' => $current_date_time,
			    ]);

        $this->add_pc_approval_record($id, $current_date_time_string);

		return redirect('/pcreport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    public function approve_7($id)
	{
	    $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'pc_status' => 8,
			    'updated_at' => $current_date_time,
			    ]);

        $this->add_pc_approval_record($id, $current_date_time_string);

		return redirect('/pcreport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    public function reject(Request $request, $id)
	{
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time_string = \Carbon\Carbon::now()->toDayDateTimeString();
        
        $report = Report::findOrFail($id);
        $report->pc_status = 9;
        $report->updated_at = $current_date_time;
        $report->pc_remarks = $request->doc_remarks;
        $report->save();

        $rejection_record = RejectionRecord::create([
            'user_id' => Auth::user()->id,
            'user_name' => Auth::user()->name,
            'remarks' => $request->doc_remarks,
            'finance_id' => $id,
            'project_code' => $report->finance_project_code,
            'project_name' => $report->finance_project_name,
            'finance_type' => 'PC',
            'finance_week' => $report->finance_week,
            'submitted_by_id' => $report->user_id,
            'submitted_by_name' => $report->user_name,
            'total' => $report->pc_total,
            'date' => $current_date_time_string,
        ]);

		return redirect('/pcreport')->with('reject_success', 'Dokumen Berhasil Di Reject!');
	}
    
    public function destroy($id)
    {
        // $finance_reports = Report::findOrFail($id);
        // $this->deletePcFile($finance_reports);
        // $finance_reports->delete();

        // return redirect('/pcreport')->with('message', 'Petty Cash Berhasil Dihapus!');
    }

    protected function add_pc_approval_record($finance_id, $current_date_time)
    {
        $id_user = Auth::user()->id;

        $approval_record = new ApprovalRecord;

        $approval_record->finance_id = $finance_id;
        $approval_record->user_id = $id_user;
        $approval_record->finance_type = 'PC';
        $approval_record->date = $current_date_time;
        $approval_record->save();
    }

    public function create_reimburse()
    {
        return view('pages.createreim');
    }

    public function index_reimburse()
    {
        $reports = DB::table('finance_reports')->get();
		return view('pages.reimreport', ['reports' => $reports]);
    }

}
