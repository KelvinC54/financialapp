<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\ReimburseReport;
use App\Models\Project;
use App\Models\ApprovalRecord;
use App\Models\Report;
use App\Models\RejectionRecord;

class ReimburseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = ReimburseReport::select(['*',])->get();
        $current_date_time = \Carbon\Carbon::now();
        
        // $reports = DB::table('finance_reports')->where('pc_type',1)->get();
		return view('pages.reimreport', ['reports' => $reports, 'current_date_time' => $current_date_time]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = collect(Project::select('*')->get());
        
        return view('pages.createreim',['projects'=>$projects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'kolom :attribute harus diisi',
        ];

        $validator = Validator::make($request->all(), [
            'reim_project_code' => 'required',
            'reim_project_name' => 'required',
            'reim_week' => 'required|numeric',
            'reim_total' => 'required',
        ], $messages);
        
        if(($request->user()->skip_approval == 1) && ($request->user()->user_location == "1") ){
            $status_reimbursement = 5;
        }
        elseif(($request->user()->skip_approval == 1) && ($request->user()->user_location == "2") ){
            $status_reimbursement = 5;
        }
        else {
            $status_reimbursement = 0;
        }

        if ($validator->fails()) {
            return redirect('/reimbursereport/create')->withErrors($validator)->withInput();
        } else {
            if($request->hasFile('reim_file')) {
                ReimburseReport::create([
                    //Meta Data
                    'project_code' => $request->reim_project_code,
                    'project_name' => $request->reim_project_name,
                    'week_reimbursement' => $request->reim_week,
                    'user_id' => $request->user_id,
                    'user_name' => $request->user_name,
                    'user_designation' => $request->user_designation,
                    // 'total_reimbursement' => $request->reim_total,
                    'total_reimbursement' => (int) filter_var($request->reim_total, FILTER_SANITIZE_NUMBER_INT),
                    'status_reimbursement' => $status_reimbursement,
                    
                    'user_remarks' => $request->doc_remarks,

                    //Files
                    'file_reimbursement' => $this->upload_reim_file($request),
                    'url_reimbursement' => $request->reim_url,
                ]);
            }
            else {
                ReimburseReport::create([
                    //Meta Data
                    'project_code' => $request->reim_project_code,
                    'project_name' => $request->reim_project_name,
                    'week_reimbursement' => $request->reim_week,
                    'user_id' => $request->user_id,
                    'user_name' => $request->user_name,
                    'user_designation' => $request->user_designation,
                    'total_reimbursement' => $request->reim_total,
                    'status_reimbursement' => $status_reimbursement,
                    'user_remarks' => $request->doc_remarks,

                    //Files
                    // 'file_reimbursement' => ,
                    'url_reimbursement' => $request->reim_url,
                ]);
            }
            return redirect('reimbursereport')->with('success','Post created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = ReimburseReport::findOrFail($id);
		return view('pages.editreim', ['report' => $report]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'kolom :attribute harus diisi',
        ];

        $validator = Validator::make($request->all(), [
            'reim_project_code' => 'required',
            'reim_project_name' => 'required',
            'reim_week' => 'required|numeric',
            'reim_total' => 'required|numeric',
        ], $messages);
        
        if(($request->user()->skip_approval == 1) && ($request->user()->user_location == "1") ){
            $status_reimbursement = 5;
        }
        elseif(($request->user()->skip_approval == 1) && ($request->user()->user_location == "2") ){
            $status_reimbursement = 4;
        }
        else {
            $status_reimbursement = 0;
        }

        if ($validator->fails()) {
            return redirect('/reimbursereport/'. $id .'/edit')->withErrors($validator)->withInput();
        } else {
            if($request->hasFile('reim_file')) {
                ReimburseReport::where('id', $id)->update([
                    //Meta Data
                    'project_code' => $request->reim_project_code,
                    'project_name' => $request->reim_project_name,
                    'week_reimbursement' => $request->reim_week,
                    'user_id' => $request->user_id,
                    'user_name' => $request->user_name,
                    'user_designation' => $request->user_designation,
                    'total_reimbursement' => $request->reim_total,
                    'status_reimbursement' => $status_reimbursement,
                    'user_remarks' => $request->doc_remarks,

                    //Files
                    'file_reimbursement' => $this->upload_reim_file($request),
                    'url_reimbursement' => $request->reim_url,
                ]);
            }
            else {
                ReimburseReport::where('id', $id)->update([
                    //Meta Data
                    'project_code' => $request->reim_project_code,
                    'project_name' => $request->reim_project_name,
                    'week_reimbursement' => $request->reim_week,
                    'user_id' => $request->user_id,
                    'user_name' => $request->user_name,
                    'user_designation' => $request->user_designation,
                    'total_reimbursement' => $request->reim_total,
                    'status_reimbursement' => $status_reimbursement,
                    'user_remarks' => $request->doc_remarks,

                    //Files
                    'url_reimbursement' => $request->reim_url,
                ]);
            }
            return redirect('reimbursereport')->with('success','Post updated successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = ReimburseReport::destroy($id);
        return redirect()->back()->with('delete_success','Report deleted!');
    }

    // Approval PMR
    public function approve_0($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $approval = ReimburseReport::where('id', $id)->update([
            'status_reimbursement' => 2,
            'updated_at' => $current_date_time,
        ]);

        $this->add_reim_approval_record($id, $current_date_time_string);

		return redirect('reimbursereport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    // Approval PMN
	public function approve_2($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $approval = ReimburseReport::where('id', $id)->update([
            'status_reimbursement' => 3,
            'updated_at' => $current_date_time,
        ]);

        $this->add_reim_approval_record($id, $current_date_time_string);

		return redirect('reimbursereport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    // Approval RCM
	public function approve_3($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $approval = ReimburseReport::where('id', $id)->update([
            'status_reimbursement' => 5,
            // Skip Apprvoal DH
            // 'status_reimbursement' => 4,
            'updated_at' => $current_date_time,
        ]);

        $this->add_reim_approval_record($id, $current_date_time_string);

		return redirect('reimbursereport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_4($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $approval = ReimburseReport::where('id', $id)->update([
            'status_reimbursement' => 5,
            'updated_at' => $current_date_time,
        ]);

        $this->add_reim_approval_record($id, $current_date_time_string);

		return redirect('reimbursereport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_5($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $approval = ReimburseReport::where('id', $id)->update([
            'status_reimbursement' => 6,
            'updated_at' => $current_date_time,
        ]);

        $this->add_reim_approval_record($id, $current_date_time_string);

		return redirect('reimbursereport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_6($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
	    
	    $document = ReimburseReport::where('id', $id)
            ->select(['total_reimbursement'])
            ->first();
            
        if ($document->total_reimbursement >= 10000000) {
            $status_reimbursement = 7;
        } else {
            $status_reimbursement = 8;
        }
        
        $approval = ReimburseReport::where('id', $id)
            ->update([
                    'status_reimbursement' => $status_reimbursement,
                    'updated_at' => $current_date_time,
                    ]);

        $this->add_reim_approval_record($id, $current_date_time_string);

		return redirect('reimbursereport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    public function approve_7($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $approval = ReimburseReport::where('id', $id)->update([
            'status_reimbursement' => 8,
            'updated_at' => $current_date_time,
        ]);

        $this->add_reim_approval_record($id, $current_date_time_string);

		return redirect('reimbursereport')->with('message', 'Dokumen Berhasil Di Approve!');
	}
	
	public function approve_8($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $approval = ReimburseReport::where('id', $id)->update([
            'status_reimbursement' => 9,
            'updated_at' => $current_date_time,
        ]);

        $this->add_reim_approval_record($id, $current_date_time_string);

		return redirect('reimbursereport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    public function reject(Request $request, $id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $report = ReimburseReport::findOrFail($id);
        $report->status_reimbursement = 1;
        $report->updated_at = $current_date_time;
        $report->remark_reimbursement = $request->doc_remarks;
        $report->save();

        $rejection_record = RejectionRecord::create([
            'user_id' => Auth::user()->id,
            'user_name' => Auth::user()->name,
            'remarks' => $request->doc_remarks,
            'finance_id' => $id,
            'project_code' => $report->project_code,
            'project_name' => $report->project_name,
            'finance_type' => 'RM',
            'finance_week' => $report->week_reimbursement,
            'submitted_by_id' => $report->user_id,
            'submitted_by_name' => $report->user_name,
            'total' => $report->total_reimbursement,
            'date' => $current_date_time_string,
        ]);

		return redirect('/reimbursereport')->with('reject_success', 'Dokumen Berhasil Di Reject!');
	}

    private function upload_reim_file($request) {
        $file = $request->reim_file;
        $filename = time() . "-reimfile-" . $request->finance_project_code . "-" . $file->getClientOriginalName();
        $uploadPath = "reimfile/";
        $file->move($uploadPath, $filename);
        return $filename;
    }

    protected function add_reim_approval_record($finance_id, $current_date_time)
    {
        $id_user = Auth::user()->id;
        
        $approval_record = new ApprovalRecord;

        $approval_record->finance_id = $finance_id;
        $approval_record->user_id = $id_user;
        $approval_record->finance_type = 'RM';
        $approval_record->date = $current_date_time;
        $approval_record->save();
    }

    
}
