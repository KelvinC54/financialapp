<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Report;
use App\Models\ApprovalRecord;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Storage;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $reports = DB::table('finance_reports')->get();
        $reimreports = DB::table('reimburse_reports')->get();
		return view('pages.dashboard', ['reports' => $reports], ['reimreports' => $reimreports]);
    }
}
