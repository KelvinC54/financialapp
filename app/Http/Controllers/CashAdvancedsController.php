<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Report;
use App\Models\ApprovalRecord;
use App\Models\Project;
use App\Models\RejectionRecord;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;
use Storage;

class CashAdvancedsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
	{
	    $reports = Report::orderBy('finance_id', 'DESC')->get();
		//$reports = DB::table('finance_reports')->get();
        $current_date_time = \Carbon\Carbon::now();

		return view('pages.careport', ['reports' => $reports, 'current_date_time' => $current_date_time]);
	}
	
	public function indexall()
	{
	    
	    $reports = Report::orderBy('finance_id', 'DESC')->get();
		//$reports = DB::table('finance_reports')->get();
        $current_date_time = \Carbon\Carbon::now();

		return view('pages.careportall', ['reports' => $reports, 'current_date_time' => $current_date_time]);
	}

    public function create()
	{
        $projects = collect(Project::select('*')->get());
        
        // $projects = collect(Project::select('*')->get())->keyBy('project_code');

        // $user_unclosed_projects = DB::table('finance_reports')->get();
            
        // $user_unclosed_projects = DB::table('finance_reports')
        //     ->where('user_id', Auth::user()->id)
        //     ->where('closed_status', '!=', 1)
        //     ->select('finance_project_code')
        //     ->get();

        // $user_unclosed_projects = DB::table('finance_reports')
        //     ->where('user_id', Auth::user()->id)
        //     ->where('closed_status', '!=', 1)
        //     ->select('finance_project_code', 'closed_status', 'multiple_ca_on_project')
        //     ->get();

        // foreach ($user_unclosed_projects as $u) {
        //     unset($projects[$u->finance_project_code]);
        
        // foreach ($user_unclosed_projects as $u) {
        //     $projects[$u->finance_project_code];
            
        //      if ($u->closed_status == 1) {
        //          continue;
        //      } elseif ($u->closed_status == 0 && $u->multiple_ca_on_project == 1) {
        //          continue;
        //      } else {
        //          unset($projects[$u->finance_project_code]);
        //      }

        // }

        return view('pages.createca',['projects'=>$projects]);
	}

    private function deleteCaFile($request) {
		$file = 'cafile/'.$request->cafile;
		if(is_file($file)) {
			unlink($file);
		}
	}

    // private function getCaFile($id){
    //     $file = "cafile/";
    //     return response()->download();

    // }

    private function upload_ca_file($request) {
        $file = $request->cafile;
        $filename = time() . "-cafile-" . $request->finance_project_code . "-" . $file->getClientOriginalName();
        $uploadPath = "cafile/";
        $request->file('cafile')->move($uploadPath, $filename);
        return $filename;
    }

    public function upload_ca_file_ajax(Request $request) {
        $file = $request->cafile;
        $filename = time() . "-cafile-" . $request->finance_project_code . "-" . $file->getClientOriginalName();
        $uploadPath = "cafile/";
        $request->file('cafile')->move($uploadPath, $filename);
        return response()->json(['success' => true,'nama_file'=>$filename]);
    }

    public function store(Request $request)
    {
        $messages = [
            'required' => 'kolom :attribute harus diisi',
        ];

        $validator = Validator::make($request->all(), [
            'finance_project_code' => 'required',
            'finance_project_name' => 'required',
            'finance_week' => 'required|numeric',
            'ca_total' => 'required|numeric',
        ], $messages);
        
        if(($request->user()->skip_approval == 1) && ($request->user()->user_location == "1") ){
            $ca_status = 5;
            $ca_rcm = $request->ca_total;
        }
        elseif(($request->user()->skip_approval == 1) && ($request->user()->user_location == "2") ){
            $ca_status = 5;
            $ca_rcm = $request->ca_total;
        }
        elseif($request->user()->skip_approval == 2){
            $ca_status = 2;
            $ca_rcm = null;
        }
        elseif($request->user()->skip_approval == 3){
            $ca_status = 3;
            $ca_rcm = null;
        }
        else {
            $ca_status = 0;
            $ca_rcm = null;
        }

        if ($validator->fails()) {
            return redirect('/careport/create')->withErrors($validator)->withInput();
        } else {
            $user_id = Auth::id();
            $current_date_time = \Carbon\Carbon::now();

            if($request->hasFile('cafile')) {
                DB::table('finance_reports')->insert([
                    // Meta Data
                    'user_id' => $user_id,
                    'user_name'=> $request->user()->name,
                    'user_designation'=> $request->user()->designation,
                    'finance_project_code' => $request->finance_project_code,
                    'finance_project_name'=> $request->finance_project_name,
                    'finance_week'=> $request->finance_week,
                    'pc_status'=> 0,
                    'updated_at' => $current_date_time,
                    'created_at' => $current_date_time,
                    'closed_status' => 0,

                    //CA
                    'ca_type' =>$request->ca_type,
                    'user_remarks' =>$request->doc_remarks,
                    'ca_total' => $request->ca_total,
                    'ca_rcm' => $ca_rcm,
                    'ca_status' => $ca_status,

                    //File CA
                    'ca_file' => $this->upload_ca_file($request),
                ]);
            }
            else {
                DB::table('finance_reports')->insert([
                    // Meta Data
                    'user_id' => $user_id,
                    'user_name'=> $request->user()->name,
                    'user_designation'=> $request->user()->designation,
                    'finance_project_code' => $request->finance_project_code,
                    'finance_project_name'=> $request->finance_project_name,
                    'finance_week'=> $request->finance_week,
                    'pc_status' => 0,
                    'updated_at' => $current_date_time,
                    'created_at' => $current_date_time,
                    'closed_status' => 0,

                    //CA
                    'ca_type' =>$request->ca_type,
                    'user_remarks' =>$request->doc_remarks,
                    'ca_total' => $request->ca_total,
                    'ca_rcm' => $ca_rcm,
                    'ca_status' => $ca_status,
                    

                    //File CA
                    'ca_url' => $request->ca_url
                ]);
            }
            return redirect('careport')->with('success','Post created successfully.');
        }
    }

    //Approval PM Reg
    public function approve_0($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'ca_status' => 2,
                'updated_at' => $current_date_time,
			    ]);

        $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/careport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    //Approval PM Nat
	public function approve_2($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
        $document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'ca_status' => 3,
                'updated_at' => $current_date_time,
			    ]);

        $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/careport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    //Approval RCM
	public function approve_3(Request $request, $id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'ca_status' => 5,
			 //   Skip Approval DH
			 //   'ca_status' => 4,
                'updated_at' => $current_date_time,
                'ca_rcm' => $request->ca_rcm,
			    ]);

        $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/careport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    //Approval DH
	public function approve_4($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'ca_status' => 5,
                'updated_at' => $current_date_time,
			]);

        $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/careport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    //Approval Akuntan Ap
	public function approve_5($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'ca_status' => 6,
                'updated_at' => $current_date_time,
			    ]);

        $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/careport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    //Approval Akuntan Head
	public function approve_6(Request $request, $id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();

        $document = DB::table('finance_reports')
            ->where('finance_id', $id)
            ->select(['ca_total'])
            ->first();

        if ($document->ca_total >= 10000000) {
            $ca_status = 7;
        } else {
            $ca_status = 8;
        }

        $update_document = DB::table('finance_reports')
            ->where('finance_id', $id)
            ->update([
                'ca_status' => $ca_status,
                'updated_at' => $current_date_time,
                'ca_ahead' => $request->ca_ahead,
                ]);

        $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/careport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    //Approval BOD
    public function approve_7(Request $request, $id)
	{
	    $ca_total = DB::table('finance_reports')->where('finance_id', $id)->value('ca_total');
	    
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'ca_status' => 8,
                'updated_at' => $current_date_time,
                'ca_bod' => $request->ca_bod,
			    ]);
			    
// 	    $document = DB::table('finance_reports')
// 			->where('finance_id', $id)
// 			->update([
// 			    'ca_status' => 8,
//                 'updated_at' => $current_date_time,
//                 'ca_bod' => $ca_total,
// 			    ]);

        $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/careport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    //Approval Akuntan Tf
    public function approve_8($id)
	{
        $current_date_time = \Carbon\Carbon::now();
	    $current_date_time_string = $current_date_time->toDayDateTimeString();
		$document = DB::table('finance_reports')
			->where('finance_id', $id)
			->update([
			    'ca_status' => 9,
                'updated_at' => $current_date_time
			    ]);

        $this->add_ca_approval_record($id, $current_date_time_string);

		return redirect('/careport')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    public function reject(Request $request, $id)
	{
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time_string = $current_date_time->toDayDateTimeString();

        $report = Report::findOrFail($id);
        $report->ca_status = 1;
        $report->updated_at = $current_date_time;
        $report->ca_remarks = $request->doc_remarks;
        $report->save();

        $rejection_record = RejectionRecord::create([
            'user_id' => Auth::user()->id,
            'user_name' => Auth::user()->name,
            'remarks' => $request->doc_remarks,
            'finance_id' => $id,
            'project_code' => $report->finance_project_code,
            'project_name' => $report->finance_project_name,
            'finance_type' => 'CA',
            'finance_week' => $report->finance_week,
            'submitted_by_id' => $report->user_id,
            'submitted_by_name' => $report->user_name,
            'total' => $report->ca_total,
            'date' => $current_date_time_string,
        ]);

		return redirect('/careport')->with('reject_success', 'Dokumen Berhasil Di Reject!');
	}   

    public function edit($id)
    {
        $finance_reports = Report::findOrFail($id);
		return view('pages.editca', ['finance_reports' => $finance_reports]);
    }

    public function update(Request $request, $id)
    {
        $finance_reports = Report::findOrFail($id);

        if($request->hasFile('ca_file')) {
            $this->deleteCaFile($finance_reports);
			$finance_reports->ca_file = $this->upload_ca_file($request);
        }
        
        if(($request->user()->skip_approval == 1) && ($request->user()->user_location == "1") ){
            $ca_status = 5;
            $ca_rcm = $request->ca_total;
        }
        elseif(($request->user()->skip_approval == 1) && ($request->user()->user_location == "2") ){
            $ca_status = 4;
            $ca_rcm = $request->ca_total;
        }
        else {
            $ca_status = 0;
            $ca_rcm = null;
        }

        $finance_reports->save();

        DB::table('finance_reports')->where('finance_id',$id)->update([
			// Meta Data
            'finance_project_code' => $request->finance_project_code,
            'finance_project_name'=> $request->finance_project_name,
            'finance_week'=> $request->finance_week,

            //CA
            'ca_type' =>$request->ca_type,
            'user_remarks' =>$request->doc_remarks,
            'ca_total' => $request->ca_total,
            'ca_status' => $ca_status,
            'ca_rcm' => $ca_rcm,

			//File CA
            'ca_url' => $request->ca_url

		]);

        return redirect()->back()->with('update_success', 'Report Berhasil diupdate!');

    }

    public function destroy($id)
    {
        $finance_reports = Report::findOrFail($id);
        $this->deleteCaFile($finance_reports);
        $finance_reports->delete();

        return redirect('/careport')->with('message', 'Cash Advanced Berhasil Dihapus!');
    }

    protected function add_ca_approval_record($finance_id, $current_date_time)
    {
        $id_user = Auth::user()->id;

        $approval_record = new ApprovalRecord;

        $approval_record->finance_id = $finance_id;
        $approval_record->user_id = $id_user;
        $approval_record->finance_type = 'CA';
        $approval_record->date = $current_date_time;
        $approval_record->save();
    }

    public function allow_multiple_ca($id)
	{
        $report = Report::findOrFail($id);
        $report->multiple_ca_on_project = 1;
        $report->save();

		return redirect('/careport')->with('allow_success', 'Allow multiple CA sukses!');
	}

    public function revoke_multiple_ca($id)
	{
        $report = Report::findOrFail($id);
        $report->multiple_ca_on_project = 0;
        $report->save();

		return redirect('/careport')->with('revoke_success', 'Revoke izin multiple CA sukses!');
	}
}
