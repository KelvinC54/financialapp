<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RejectionController extends Controller
{
    public function index($type)
	{   
		$finance_type = '';
		switch ($type) {
			case 'ca':
				$finance_type = 'CA';
				break;
			case 'pc':
				$finance_type = 'PC';
				break;
			case 'rm':
				$finance_type = 'RM';
				break;
			default:
				break;
		}

		$projects = collect(
			DB::table('rejection_records')
			->where('finance_type', $finance_type)
			->select('project_code', 'project_name')
			->get()
			)->keyBy('project_code');

		$rejection = DB::table('rejection_records')
			->where('finance_type', $finance_type)
			->select(
				'*',
			)
			->get();

		// dd($rejection);


		// dd($reject);

		return view('pages.rejection',[
			'projects'=>$projects,
			'reports'=>$rejection,
			'finance_type'=>$type,
		]);
	}
}
