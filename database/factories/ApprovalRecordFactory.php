<?php

namespace Database\Factories;

use App\Models\ApprovalRecord;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ApprovalRecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ApprovalRecord::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'finance_id' => 25,
            'user_id' => 1,
            'finance_type' => 'PC',
            'date' => 'Thu, Mar 20, 2021 11:00 AM'
        ];
    }
}
