<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReimburseReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reimburse_reports', function (Blueprint $table) {
            $table->id();
            $table->string('project_code');
            $table->string('project_name');
            $table->string('week_reimbursement');
            $table->bigInteger('user_id');
            $table->string('user_name');
            $table->string('user_designation');
            $table->integer('total_reimbursement');
            $table->string('file_reimbursement')->nullable();
            $table->string('url_reimbursement')->nullable();
            $table->text('remark_reimbursement')->nullable();
            $table->integer('status_reimbursement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reimburse_reports');
    }
}
