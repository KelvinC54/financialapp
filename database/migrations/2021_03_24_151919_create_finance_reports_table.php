<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance_reports', function (Blueprint $table) {
            //Meta Data
            $table->id('finance_id');
            $table->integer('user_id');
            $table->string('user_name');
            $table->string('user_designation');
            $table->string('finance_project_code');
            $table->string('finance_project_name');
            $table->string('finance_week');
            $table->timestamps();
            //CA
            $table->integer('ca_total');
            $table->string('ca_transfered');
            $table->string('ca_aging');
            $table->integer('ca_status');
            //PC
            $table->integer('pc_total');
            $table->string('pc_receipt_back_from_ca');
            $table->integer('pc_status');
            //Files
            $table->string('ca_file');
            $table->string('ca_url');
            $table->string('pc_file');
            $table->string('pc_url');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance_reports');
    }
}
