<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Financial App</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition sidebar-mini">

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap 4 -->
    <script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- DataTables  & Plugins -->
    <script src="{{ asset('lte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

    <!-- Bootstrap 4 -->
    <script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Adminlte App -->
    <script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>

    <script>
div.b {
  word-wrap: break-word;
}
</script>

    <div class="wrapper">
        <div>
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                                class="fas fa-bars"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Right navbar links -->
        <div>
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <div>
                    <a href="/careport" class="brand-link">
                        <img src="{{ asset('weblogo.png') }}" alt="Web Logo"
                            class="brand-image img-circle elevation-3" style="opacity: .8">
                        <span class="brand-text font-weight-light">Finance Tools</span>
                    </a>
                </div>
                <!-- Nama User -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 ">
                        <div class="image">
                            <!--<img src="{{ asset('lte/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"-->
                            <!--    alt="User Image">-->
                               
                                    {{-- <h3 style="color:#ffffff;">{{ Auth::user()->name }}</h3> --}}
                           
                        </div>
                        <div class="info">
                            <a  class="d-block" href="{{ route('user.profile') }}" >
                                <div class="row">
                                <div class="b">
                                        {{ Auth::user()->name }} 
                                    </div>
                                    <div class="col text-right">
                                        <i class="fas fa-cog"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- SidebarSearch Form -->
                    <div class="form-inline">
                        <div class="input-group" data-widget="sidebar-search">
                            <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                                aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-sidebar">
                                    <i class="fas fa-search fa-fw"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <!-- Sidebar Menu -->
                        <nav class="mt-2">
                            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                                data-accordion="false">
                                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                                <!-- <li class="nav-item menu-open">
                                    <a href="/home" class="nav-link ">
                                        <i class="nav-icon"></i>
                                        <p>
                                            Dashboard
                                            <i class="right"></i>
                                        </p>
                                    </a>
                                </li> -->
                                <li class="nav-item menu-open">
                                    <a href="#" class="nav-link active">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>
                                            Cash Advance
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/careport" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>CA Report</p>
                                            </a>
                                        </li>
                                    </ul>
                                    @if (Auth::user()->jobdesk == 'webadmin' || 
                                        Auth::user()->jobdesk == '3' || Auth::user()->jobdesk == '4' ||
                                        Auth::user()->jobdesk == '6' ||Auth::user()->jobdesk == '7' ||
                                        Auth::user()->jobdesk == '8' || Auth::user()->jobdesk == '9')
                                        <ul class="nav nav-treeview">
                                            <li class="nav-item">
                                                <a href="/careportall" class="nav-link active">
                                                    <i class="far fa-circle nav-icon"></i>
                                                    <p>All CA Report</p>
                                                </a>
                                            </li>
                                        </ul>
                                    @endif
                                </li>
                                <li class="nav-item menu-open">
                                    <a href="#" class="nav-link active">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>
                                            Petty Cash
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/pcreport" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>PC Report</p>
                                            </a>
                                        </li>
                                    </ul>
                                    @if (Auth::user()->jobdesk == 'webadmin' ||
                                        Auth::user()->jobdesk == '3' || Auth::user()->jobdesk == '4' ||
                                        Auth::user()->jobdesk == '6' ||Auth::user()->jobdesk == '7' ||
                                        Auth::user()->jobdesk == '8' || Auth::user()->jobdesk == '9')
                                        <ul class="nav nav-treeview">
                                            <li class="nav-item">
                                                <a href="/pcreportall" class="nav-link active">
                                                    <i class="far fa-circle nav-icon"></i>
                                                    <p>All PC Report</p>
                                                </a>
                                            </li>
                                        </ul>
                                     @endif
                                </li>
                                <li class="nav-item menu-open">
                                    <a href="#" class="nav-link active">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>
                                            Reimbursement
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/reimbursereport" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Reimbursement Report</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item menu-open">
                                    <a href="#" class="nav-link active">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>
                                            Payment Request
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/payreqreport" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Payment Request Report</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                @if (Auth::user()->jobdesk == '2' || Auth::user()->jobdesk == '3' || Auth::user()->jobdesk == '4' || Auth::user()->jobdesk == '6' || Auth::user()->jobdesk == '7' || Auth::user()->jobdesk == '9' || Auth::user()->jobdesk == 'webadmin')
                                <li class="nav-item menu-open">
                                    <a href="#" class="nav-link active">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>
                                            Finance Report
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/finreport" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Report</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                                @if (Auth::user()->jobdesk > '1' || Auth::user()->jobdesk == 'webadmin')
                                <li class="nav-item menu-open">
                                    <a href="#" class="nav-link active">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>
                                            Approval
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/approval/ca" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>CA Approvals</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/approval/pc" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>PC Approvals</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/approval/rm" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>RM Approvals</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                                <li class="nav-item menu-open">
                                    <a href="#" class="nav-link active">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>
                                            Rejection
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/rejections/ca" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>CA Rejection</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/rejections/pc" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>PC Rejection</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/rejections/rm" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>RM Rejection</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                @if (Auth::user()->jobdesk == 'webadmin')
                                <li class="nav-item menu-open">
                                    <a href="#" class="nav-link active">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>
                                            Users
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/user" class="nav-link active">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Users Data</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                                <p>
                                    <br>
                                    <a class="btn btn-danger btn-md" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                      <span class="glyphicon glyphicon-log-out"></span> Log out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                  </p> 

                        <!--  <div class="info">
                            <a  class="d-block"
                                href="{{ route('logout') }}"
                                onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    {{ Auth::user()->name }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>-->

                            </ul>
                        </nav>
                        <!-- /.sidebar-menu -->
                    </div>
                    <!-- /.sidebar -->
            </aside>
        </div>
        <!-- /.navbar -->
        <!-- Main Content -->
        @yield('main-content')
        <div>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
                <div class="p-3">
                    <h5>Title</h5>
                    <p>Sidebar content</p>
                </div>
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <div>
            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- To the right
                <div class="float-right d-none d-sm-inline"> Anything you want -->
                <!-- Default to the left -->
                <strong>Copyright &copy; 2021 >Velacom</a>.</strong> All rights reserved.<br>Ver. 1.8.0
            </footer>
        </div>
    </div>
</body>
</html>

