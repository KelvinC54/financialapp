@extends('layouts.main')

@section('title','finreport')

@section('main-content')

<style type="text/css">
.step {
	display: none;
}
.step.active {
	display: block;
}
</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Finance Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Finance</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <!-- /.card -->
                <!-- /.card-header -->
                <div class="card-body table-responsive p-2">
                  <table id="finance-table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Date Created</th>
                      <th>Project Code</th>
                      <th>Project Name</th>
                      <th>Type CA</th>
                      <th>Week CA</th>
                      <th>PIC</th>
                      <th>Total CA</th>
                      <th>CA Approved RCM</th>
                      <th>CA Approved Ak. Head</th>
                      <th>CA Approved BOD</th>
                      <th>Status CA</th>
                      <th>Total PC</th>
                      <th>Sisa CA</th>
                      <th>Status PC</th>
                      <th>Note</th>
                      <th>Finance Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($reports as $r)
                      @if (
                          (Auth::user()->jobdesk == "webadmin" && $r->ca_status >= 0) ||
                        //   (Auth::user()->jobdesk == "1" && $r->user_id == Auth::user()->id) ||
                          (Auth::user()->jobdesk == "2") ||
                          (Auth::user()->jobdesk == "3") ||
                        //   (Auth::user()->jobdesk == "3" && $r->ca_status >= 2) ||
                          (Auth::user()->jobdesk == "4") ||
                        //   (Auth::user()->jobdesk == "5" && $r->ca_status >= 4) ||
                          (Auth::user()->jobdesk == "6") ||
                          (Auth::user()->jobdesk == "7") ||
                          (Auth::user()->jobdesk == "9")
                          )
                      <tr>
                          <td>{{$r->finance_id}}</td>
                          <td>{{$r->created_at}}</td>
                          <td>{{$r->finance_project_code}}</td>
                          <td>{{$r->finance_project_name}}</td>
                          <td>{{$r->ca_type}}</td>
                          <td>{{$r->finance_week}}</td>
                          <td>{{$r->user_name}}</td>

                          <!--{{-- view CA Total --}}-->
                          <!--@if ($r->ca_rcm == null && $r->ca_ahead == null && $r->ca_bod == null)-->
                          <!--  <td>Rp.{{number_format($r->ca_total, 0, '','.')}}</td>-->

                          <!--{{-- view CA RCM --}}-->
                          <!--@elseif ($r->ca_rcm != null  && $r->ca_ahead == null && $r->ca_bod == null)-->
                          <!--  <td>Rp.{{number_format($r->ca_rcm, 0, '','.')}}</td>-->

                          <!--{{-- view CA A Head --}}-->
                          <!--@elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod == null)-->
                          <!--  <td>Rp.{{number_format($r->ca_ahead, 0, '','.')}}</td>-->

                          <!--{{-- view CA BOD --}}-->
                          <!--@elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod != null)-->
                          <!--  <td>Rp.{{number_format($r->ca_bod, 0, '','.')}}</td>-->
                          <!--@endif-->
                          <td>Rp.{{number_format($r->ca_total, 0, '','.')}}</td>
                          <td>Rp.{{number_format($r->ca_rcm, 0, '','.')}}</td>
                          <td>Rp.{{number_format($r->ca_ahead, 0, '','.')}}</td>
                          <td>Rp.{{number_format($r->ca_bod, 0, '','.')}}</td>
                          
                          @if($r->ca_total >= 10000000)
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by BOD";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                case '10':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                            </td>
                          @else
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                case '10':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                            </td>
                          @endif
                          
                          @if ($r->pc_total == 0)
                            <td>Rp.{{number_format($r->pc_total, 0, '','.')}}</td>
                            {{-- view CA Total --}}
                            @if ($r->ca_rcm == null && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_total - $r->pc_total, 0, '','.')}}</td>

                            {{-- view CA RCM --}}
                            @elseif ($r->ca_rcm != null  && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_rcm - $r->pc_total, 0, '','.')}}</td>

                            {{-- view CA A Head --}}
                            @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_ahead - $r->pc_total, 0, '','.')}}</td>

                            {{-- view CA BOD --}}
                            @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod != null)
                            <td>Rp.{{number_format($r->ca_bod - $r->pc_total, 0, '','.')}}</td>
                            @endif
                          
                          @elseif ($r->pc_total != null)
                            <td>Rp.{{number_format($r->pc_total, 0, '','.')}}</td>
                            {{-- view CA Total --}}
                            @if ($r->ca_rcm == null && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_total - $r->pc_total, 0, '','.')}}</td>

                            {{-- view CA RCM --}}
                            @elseif ($r->ca_rcm != null  && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_rcm - $r->pc_total, 0, '','.')}}</td>

                            {{-- view CA A Head --}}
                            @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_ahead - $r->pc_total, 0, '','.')}}</td>

                            {{-- view CA BOD --}}
                            @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod != null)
                            <td>Rp.{{number_format($r->ca_bod - $r->pc_total, 0, '','.')}}</td>
                            @endif
                          @else
                            <td>-</td>
                            <td>-</td>
                          @endif

                          <td>
                            <?php
                              switch ($r->pc_status) {
                                case '1':
                                    echo "Belum di Review";
                                    break;
                                case '9':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    echo "-";
                                    break;
                              }
                            ?>
                          </td>
                          <td>{{$r->transfer_remarks}}</td>
                          <td>
                            <?php
                            switch ($r->closed_status) {
                              case '1':
                                  echo "CLOSED";
                                  break;
                              case '0':
                                  echo "OPEN";
                                  break;
                              default:
                                  echo "-";
                                  break;
                            }
                          ?></td>
                          <td>
                            <!-- Call to action buttons -->
                            <ul class="list-inline m-0">
                              
                              {{-- button VIEW URL / FILE --}}
                              <li class="list-inline-item">
                                @if ($r->ca_file == null)
                                  <a target="_blank" href="{{url('//' . $r->ca_url) }}">
                                    <button type="button" class="btn btn-warning">View CA</button>
                                  </a>
                                @else
                                  <a target="_blank" href="{{ asset('cafile/' . $r->ca_file) }}">
                                    <button type="button" class="btn btn-warning">Download CA</button>
                                  </a>
                                @endif
                              </li>

                              {{-- button VIEW URL / FILE PC --}}
                              <li class="list-inline-item">
                                @if ($r->pc_url != null)
                                  <a target="_blank" href="{{url($r->pc_url)}}">
                                    <button type="button" class="btn btn-success">View PC</button>
                                  </a>
                                @else
                                <?php
                                $pc_files = explode(",", $r->pc_file);
                                ?>
                                  @foreach ($pc_files as $file)
                                    <a target="_blank" href="{{ asset('pcfile/' . $file) }}">
                                      <button type="button" class="btn btn-success">Download PC - {{$loop->iteration}}</button>
                                    </a>
                                  @endforeach
                                @endif
                              </li>
                              
                              {{-- <li class="list-inline-item">
                                <button type="button" class="btn btn-secondary">Remarks</button></li> --}}
                                
                              @if(Auth::user()->jobdesk == "webadmin")
                                <li class="list-inline-item">
                                    <a href="/finreport/status/close/{{$r->finance_id}}">
                                    <button type="button" class="btn btn-danger">CLOSE</button>
                                    </a>
                                </li>
                              @elseif (Auth::user()->jobdesk == "9" && $r->pc_status == 8 && $r->closed_status == 0)
                                <li class="list-inline-item">
                                    <a href="/finreport/status/close/{{$r->finance_id}}">
                                    <button type="button" class="btn btn-danger">CLOSE</button>
                                    </a>
                                </li>
                              @elseif((Auth::user()->jobdesk == "9" && $r->pc_status < 8 ) || (Auth::user()->jobdesk == "9" && $r->closed_status == 1))
                                <li class="list-inline-item">
                                    <a href="/finreport/status/close/{{$r->finance_id}}">
                                    <button type="button" class="btn btn-danger" disabled>CLOSE</button>
                                    </a>
                                </li>
                              @endif


                              {{-- <li class="list-inline-item">
                                <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit CA"><i class="fa fa-edit"></i></button>
                              </li>
                              <li class="list-inline-item">
                                <button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete CA"><i class="fa fa-trash"></i></button>
                              </li> --}}
                            </ul>
                          </td>
                      </tr>
                      @endif
                      @endforeach
                  </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script>
$(document).ready(function() {
  var table = $("#finance-table").DataTable({
      // responsive: true, 
      lengthChange: false,
      autoWidth:false,
      buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
      dom: 'Bfrtip',
        } );
    });
</script>
@endsection
