@extends('layouts.main')

@section('title','Input Reimbursement')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Input Reimbursement</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Reimbursement</a></li>
              <li class="breadcrumb-item active">Input</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->

            <!-- /.card -->

              <div>
              <div class="card-body" id="settings">
                <form class="form-horizontal" action="{{ route('reimbursereport.store') }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Code</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="reim_project_code" name="reim_project_code" placeholder="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="reim_project_name" name="reim_project_name" placeholder="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Week Reimbursement</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="reim_week" name="reim_week" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_id" class="col-sm-2 col-form-label">Submitted by NIP | Name</label>
                    <div class="col-sm-10">
                        <input id="user_id" type="text" name="user_id" value="{{ Auth::user()->nip }}" readonly>
                        <input id="user_name" type="text" name="user_name" value="{{ Auth::user()->name }}" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_designation"class="col-sm-2 col-form-label">Designation</label>
                    <div class="col-sm-10">
                      <input id="user_designation" type="text" name="user_designation" value="{{ Auth::user()->designation }}" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Total Reimbursement</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="reim_total" name="reim_total" placeholder="ex: 1000000 (Hanya angka tanpa titik dan Rupiah)">
                      <!--<input type="text" class="form-control" data-inputmask="'alias': 'currency', 'groupSeparator': '.', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': 'Rp. ', 'placeholder': '0'" id="reim_total" name="reim_total">-->
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pilih Cara Upload File Reimbursement</label>
                    <div class="col-sm-10">
                      <label class="col-form-label pb-3">
                        <input class="upload_option" type="radio" name="upload_option" value="URL" checked> URL
                      </label>
                      <br>
                      <label class="col-form-label pb-3">
                        <input class="upload_option" type="radio"name="upload_option" value="FILE"> Upload File
                      </label>
                    </div>
                  </div>

                  <div class="form-group row use-file" style="display:none;">
                    <label class="col-sm-2 col-form-label">File Reimbursement</label>
                    <div class="col-sm-10">
                        <input type="file" name="reim_file">
                    </div>
                  </div>

                  <div class="form-group row use-url">
                    <label class="col-sm-2 col-form-label">URL File Reimbursement</label>
                    <div class="col-sm-10">
                      <input id="reim_url" type="text" name="reim_url" placeholder="www.url.com/subdomain" />
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Remark Reimbursement From Creator</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="10" placeholder="Inputkan Catatan untuk Reimbursement yang diajukan" required></textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- Sweet ALert -->
<link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Sweet Alert -->
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
<input class="number">
<script>
    $("input[type='file']").on("change", function() {
        if (this.files[0].size > 100000000) {
          Swal.fire({
                      icon: 'error',
                      title: 'Ukuran File Terlalu Besar',
                      text: 'Silakan upload file dengan ukuran kurang dari 10MB',
              });
          this.value = '';
          return false;
        } else {
          var pathFile = this.value;
          var ekstensiOk = /(\.jpg|\.jpeg|\.png|\.doc|\.docx|\.xlsx|\.xls|\.txt|\.pdf|\.zip|\.csv)$/i;
          if(!ekstensiOk.exec(pathFile)){
              Swal.fire({
                      icon: 'error',
                      title: 'Ekstensi File Salah!',
                      text: 'Silakan upload file dengan ekstensi jpg,jpeg,png,doc,docx,xlsx,xls,txt,pdf,zip',
              });
              this.value = '';
              return false;
          }else{
            // create CA tanpa ajax
            Swal.fire({
                    icon:'success',
                    title: 'File Benar!',
                    text: 'Ukuran dan ekstensi file sudah tepat',
                    showConfirmButton: true,
              });
            
            // create CA with ajax
            // var ekstensiPdf = /(\.pdf)$/i;
            // if (ekstensiOk.exec(pathFile)) {
            //   uploadFileLive('pdf');
            // } else {
            //   uploadFileLive('others');
            // }          
          }
        }
    });
    
    $(document).ready(function(){
        $("input").inputmask();
    });

    // $('#reim_total').keyup(function(event) {
    
    //   // skip for arrow keys
    //   if(event.which >= 37 && event.which <= 40) return;
    
    //   // format number
    //   $(this).val(function(index, value) {
    //     return value
    //     .replace(/\D/g, "")
    //     .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    //     ;
    //   });
    // });

// var rupiah = document.getElementById('reim_total');
// 		rupiah.addEventListener('keyup', function(e){
// 			// tambahkan 'Rp.' pada saat form di ketik
// 			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
// 			rupiah.value = formatRupiah(this.value, 'Rp. ');
// 		});
 
// 		/* Fungsi formatRupiah */
// 		function formatRupiah(angka, prefix){
// 			var number_string = angka.replace(/[^,\d]/g, '').toString(),
// 			split   		= number_string.split(','),
// 			sisa     		= split[0].length % 3,
// 			rupiah     		= split[0].substr(0, sisa),
// 			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
// 			// tambahkan titik jika yang di input sudah menjadi angka ribuan
// 			if(ribuan){
// 				separator = sisa ? '.' : '';
// 				rupiah += separator + ribuan.join('.');
// 			}
 
// 			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
// 			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
// 		}
		
    $('.upload_option').on('click', function() {
        var upload_option = $('input[name="upload_option"]:checked').val();
        if (upload_option == 'URL') {
            $('.use-url').show();
            $('.use-file').hide();
        } else if (upload_option == 'FILE'){
            $('.use-url').hide();
            $('.use-file').show();
        }
    });
    
   
</script>
@endsection
