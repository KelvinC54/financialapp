@extends('layouts.main')

@section('title','pc_report')

@section('main-content')

<style type="text/css">
.step {
	display: none;
}
.step.active {
	display: block;
}
</style>
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Petty Cash Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Petty Cash</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <!-- /.card -->
              <div class="card">
                <div class="card-header col-md-12">
                  @if(Session::has('delete_success'))
                    <div class="alert alert-warning">
                        {{Session::get('delete_success')}}
                    </div>
                  @endif
                  @if(Session::has('reject_success'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                      {{Session::get('reject_success')}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  @endif
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-2">
                  <table id="pc-table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Date Created</th>
                      <th>Date Updated</th>
                      <th>Project Code</th>
                      <th>Project Name</th>
                      <th>Type CA</th>
                      <th>Week CA</th>
                      <th>Submited By</th>
                      <th>Total CA</th>
                      <th>Total PC</th>
                      <th>Sisa CA</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($reports as $r)
                      @if (
                        (Auth::user()->jobdesk == "webadmin" && $r->pc_status >= 1) ||
                        ((Auth::user()->jobdesk == "1" && $r->user_id == Auth::user()->id) && $r->pc_status >= 1) ||
                        (Auth::user()->jobdesk == "2" && $r->pc_status >= 1) ||
                        (Auth::user()->jobdesk == "3" && $r->pc_status == 2) ||
                        (Auth::user()->jobdesk == "4" && $r->pc_status == 3) ||
                        (Auth::user()->jobdesk == "5" && $r->pc_status >= 4) ||
                        (Auth::user()->jobdesk == "6" && $r->pc_status >= 1) ||
                        (Auth::user()->jobdesk == "7" && $r->pc_status == 6) ||
                        (Auth::user()->jobdesk == "8" && $r->pc_status >= 7) ||
                        (Auth::user()->jobdesk == "9" && $r->pc_status == 7)
                        )
                      <tr>
                          <td>{{$r->finance_id}}</td>
                          <td>{{$r->created_at}}</td>
                          <td>{{$r->updated_at}}</td>
                          <td>{{$r->finance_project_code}}</td>
                          <td>{{$r->finance_project_name}}</td>
                          <td>{{$r->ca_type}}</td>
                          <td>{{$r->finance_week}}</td>
                          <td>{{$r->user_name}}</td>
                          {{-- view CA Total --}}
                          @if ($r->ca_rcm == null && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_total, 0, '','.')}}</td>

                          {{-- view CA RCM --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_rcm, 0, '','.')}}</td>

                          {{-- view CA A Head --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_ahead, 0, '','.')}}</td>

                          {{-- view CA BOD --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod != null)
                            <td>Rp.{{number_format($r->ca_bod, 0, '','.')}}</td>
                          @endif
                          
                          <td>Rp.{{number_format($r->pc_total, 0, '','.')}}</td>
                          
                          {{-- view CA Total - PC Total --}}
                          @if ($r->ca_rcm == null && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_total - $r->pc_total, 0, '','.')}}</td>

                          {{-- view CA RCM - PC Total --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_rcm - $r->pc_total, 0, '','.')}}</td>

                          {{-- view CA A Head - PC Total --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_ahead - $r->pc_total, 0, '','.')}}</td>

                          {{-- view CA BOD - PC Total --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod != null)
                            <td>Rp.{{number_format($r->ca_bod - $r->pc_total, 0, '','.')}}</td>
                          @endif
                        
                          <td><?php
                            switch ($r->pc_status) {
                                case '1':
                                    echo "Belum di Review";
                                    break;
                                case '9':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by Akuntan Transfer";
                                    break;
                                default:

                                    break;
                            }
                            ?>
                          </td>
                        <td>
                          <!-- Call to action buttons -->
                          <ul class="list-inline m-0">
                              
                            {{-- button VIEW USER REMARK --}}
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#user_remarks_{{ $r->finance_id }}">Notes PC</button>
                          </li>
                          <div id="user_remarks_{{ $r->finance_id }}" class="modal fade">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remarks from {{ $r->user_name }}:</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>{{ $r->user_remarks_pc }}</p>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                    </div>
                                </div>
                            </div>
                          </div>

                            {{-- button VIEW URL / FILE CA --}}
                          <li class="list-inline-item">
                            @if ($r->ca_file == null)
                              <a target="_blank" href="{{url('//' . $r->ca_url) }}">
                                <button type="button" class="btn btn-warning btn-sm">View CA</button>
                              </a>
                            @else
                              <a target="_blank" href="{{ asset('cafile/' . $r->ca_file) }}">
                                <button type="button" class="btn btn-success btn-sm">Download CA</button>
                              </a>
                            @endif
                          </li>

                            {{-- button VIEW URL / FILE PC --}}
                            <li class="list-inline-item">
                              @if ($r->pc_url != null)
                                <a target="_blank" href="{{url($r->pc_url)}}">
                                  <button type="button" class="btn btn-primary btn-sm">View PC</button>
                                </a>
                              @else
                              <?php
                              $pc_files = explode(",", $r->pc_file);
                              ?>
                                @foreach ($pc_files as $file)
                                  <a target="_blank" href="{{ asset('pcfile/' . $file) }}">
                                    <button type="button" class="btn btn-warning btn-sm">Download PC - {{$loop->iteration}}</button>
                                  </a>
                                @endforeach
                              @endif
                            </li>
                            
                             {{-- button VIEW URL / FILE NOTA--}}
                              <li class="list-inline-item">
                                @if ($r->pc_receipt_back_from_ca != null)
                                    <a target="_blank" href="{{ asset('pc_receipt_back_from_ca/' . $r->pc_receipt_back_from_ca) }}">
                                        <button type="button" class="btn btn-info btn-sm">View Nota </button>
                                    </a>
                                @endif
                              </li>

                            {{-- button REMARKS PC --}}
                            @if ($r->pc_status == 9)
                            <li class="list-inline-item">
                              <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#remarks_{{ $r->finance_id }}">Remarks</button>
                            </li>

                            <div id="remarks_{{ $r->finance_id }}" class="modal fade">
                              <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h4 class="modal-title">PC Remarks:</h4>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                      <div class="modal-body">
                                          <p>{{ $r->pc_remarks }}</p>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          @endif

                            {{-- button EDIT, button DELETE --}}
                            @if (($r->pc_status <= 1 && $r->ca_status == 10) && (Auth::user()->jobdesk == "1" || Auth::user()->jobdesk == "webadmin") || ($r->pc_status == 9 && $r->ca_status == 10) && (Auth::user()->jobdesk == "1" || Auth::user()->jobdesk == "webadmin"))

                              {{-- button EDIT --}}
                              <li class="list-inline-item">
                                <a href="{{ url('pcreport/'. $r ->finance_id .'/edit') }}">
                                  <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit PC"><i class="fa fa-edit"></i></button>
                                </a>
                              </li>

                              <!--{{-- button DELETE --}}-->
                              <!--<li class="list-inline-item">-->
                              <!--  <form action="pcreport/{{ $r->finance_id }}" method="POST">-->
                              <!--    <input type="hidden" name="_method" value="delete">-->
                              <!--    <input type="hidden" name="_token" value="{{ csrf_token() }}">-->
                              <!--      <button class="btn btn-danger btn-sm rounded-0" type="submit" data-toggle="tooltip" data-placement="top" title="Delete CA"><i class="fa fa-trash"></i></button>-->
                              <!--  </form>-->
                              <!--</li>-->

                            @endif

                            {{-- button ACCEPT, button REJECT --}}

                            {{-- PM Reg approve & reject --}}
                            @if (Auth::user()->jobdesk == "2" && $r->pc_status == 1)
                              <li class="list-inline-item">
                                <a href="{{ url('pcreport/approve-1/' . $r->finance_id) }}">
                                  <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                                </a>
                              </li>
                              <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pm-valid-{{ $r->finance_id }}" data-placement="top" title="Reject PC"><i class="fas fa-window-close"></i></button>
                              
                                <div id="reject-pm-valid-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('pcreport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject PC</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </li>

                            {{-- PM National approve & reject --}}
                            @elseif (Auth::user()->jobdesk == "3" && $r->pc_status == 2)
                              <li class="list-inline-item">
                                <a href="{{ url('pcreport/approve-2/' . $r->finance_id) }}">
                                  <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                                </a>
                              </li>
                              <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pm-national-{{ $r->finance_id }}" data-placement="top" title="Reject PC"><i class="fas fa-window-close"></i></button>
                              
                                <div id="reject-pm-national-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('pcreport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject PC</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </li>

                            {{-- RCM approve & reject --}}
                            @elseif (Auth::user()->jobdesk == "4" && $r->pc_status == 3)
                              <li class="list-inline-item">
                                <a href="{{ url('pcreport/approve-3/' . $r->finance_id) }}">
                                  <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                                </a>
                              </li>
                              <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pcm-valid-{{ $r->finance_id }}" data-placement="top" title="Reject PC"><i class="fas fa-window-close"></i></button>
                              
                                <div id="reject-pcm-valid-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('pcreport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject PC</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </li>

                            {{-- DH approve & reject --}}
                            @elseif (Auth::user()->jobdesk == "5" && $r->pc_status == 4)
                              <li class="list-inline-item">
                                <a href="{{ url('pcreport/approve-4/' . $r->finance_id) }}">
                                  <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                                </a>
                              </li>
                              <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pd-{{ $r->finance_id }}" data-placement="top" title="Reject PC"><i class="fas fa-window-close"></i></button>
                              
                                <div id="reject-pd-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('pcreport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject PC</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </li>

                            {{-- Akuntan Approval approve & reject --}}
                            @elseif (Auth::user()->jobdesk == "6" && $r->pc_status == 5)
                              <li class="list-inline-item">
                                <a href="{{ url('pcreport/approve-5/' . $r->finance_id) }}">
                                  <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                                </a>
                              </li>
                              <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-{{ $r->finance_id }}" data-placement="top" title="Reject PC"><i class="fas fa-window-close"></i></button>
                              
                                <div id="reject-akun-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('pcreport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject PC</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </li>

                            {{-- Akuntan Head approve & reject --}}
                            @elseif (Auth::user()->jobdesk == "7" && $r->pc_status == 6)
                              <li class="list-inline-item">
                                <a href="{{ url('pcreport/approve-6/' . $r->finance_id) }}">
                                  <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                                </a>
                              </li>
                              <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-head-{{ $r->finance_id }}" data-placement="top" title="Reject PC"><i class="fas fa-window-close"></i></button>
                              
                                <div id="reject-akun-head-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('pcreport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject PC</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </li>

                            {{-- Akuntan Transfer approve & reject --}}
                            @elseif (Auth::user()->jobdesk == "9" && $r->pc_status == 7)
                              <li class="list-inline-item">
                                <a href="{{ url('pcreport/approve-7/' . $r->finance_id) }}">
                                  <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                                </a>
                              </li>
                              <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-tf-{{ $r->finance_id }}" data-placement="top" title="Reject PC"><i class="fas fa-window-close"></i></button>
                              
                                <div id="reject-akun-tf-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('pcreport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject PC</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </li>
                            @endif
                          </ul>
                        </td>
                      </tr>
                      @endif
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                          <th></th>
                          <th>Date Created</th>
                          <th>Project Code</th>
                          <th>Project Name</th>
                          <th>Type CA</th>
                          <th>Week CA</th>
                          <th>Submitted By</th>
                          <th>Total CA</th>
                          <th>Total PC</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script>
  var table = $("#pc-table").DataTable({
    // responsive: true,
    order: [[0, 'desc']],
    lengthChange: false, 
    autoWidth:false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    dom: 'Bfrtip',
  });

  $('#pc-table tfoot th').each( function (i) {
      var title = $('#pc-table thead th').eq( $(this).index() ).text();
      $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" data-index="'+i+'" />' );
  } );

  $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
    table
          .column( $(this).data('index') )
          .search( this.value )
          .draw();
  } );
</script>
@endsection
