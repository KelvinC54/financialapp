@extends('layouts.main')

@section('title','Dashboard')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">

            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <!-- /.card -->
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title"></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <div class="card-body table-responsive p-0">
                        <table id="dashboard-table" class="table table-hover text-nowrap">
                          <thead>
                            <tr>
                              <th>Project ID</th>
                              <th>Project Name</th>
                              <th>Grand Total CA</th>
                              <th>Grand Total PC</th>
                              <th>Approval</th>
                              <th>Rejected</th>
                              <th>Closing</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>183</td>
                              <td>Battery</td>
                              <td>Rp 1000.000</td>
                              <td>Rp 500.000</td>
                              <td>50</td>
                              <td>10</td>
                              <th>100</th>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->
<!-- REQUIRED SCRIPTS -->
<script>
      $("#dashboard-table").DataTable({
      responsive: true,
      lengthChange: false,
      autoWidth:false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      dom: 'frti',
    });
</script>
@endsection
