@extends('layouts.main')

@section('title','payreqreport')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->                                   
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Payment Request Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Payment Request</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    
    @if(Auth::user()->jobdesk != "webadmin")
        <div style="text-align: center">
            <br><br><br><br><br>
            <h1><b>This Feature Still Under Maintenance</b></h1>
            <h3>Pardon Our Dust :)</h3>
            <br><br><br><br><br>
            <h4>Fitur Payment Request untuk sementara ini dapat menggunakan fitur CA</h4>
        </div>
   @else
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->

            <!-- /.card -->

            <div class="card">
              <div class="card-header col-md-12">
                @if(Session::has('delete_success'))
                <div class="alert alert-warning">
                    {{Session::get('delete_success')}}
                </div>
                @endif
                @if(Session::has('reject_success'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  {{Session::get('reject_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                {{-- @if(Session::has('allow_success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{Session::get('allow_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if(Session::has('revoke_success'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  {{Session::get('revoke_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif --}}
                <a href="{{ route('payreqreport.create') }}">
                <button col-md-12 type="button" class="btn btn-outline-primary">Add New Payment Request</button></a>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-2">
                <table id="payreq-table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Date Created</th>
                    <th>Project Code</th>
                    <th>Project Name</th>
                    <th>Type Payment Request</th>
                    <th>Week</th>
                    <th>Submited By</th>
                    <th>Total Payment Request</th>
                    <th>Status</th>
                    <th>Aging Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($reports as $r)
                  @if (
                       (Auth::user()->jobdesk == "webadmin" && $r->payreq_status >= 0) //||
                      // ((Auth::user()->jobdesk == "1" && $r->user_id == Auth::user()->id) && $r->payreq_status < 10) ||
                      // ((Auth::user()->jobdesk == "2" && $r->payreq_status == 0) || (Auth::user()->jobdesk == "2" && $r->user_id == Auth::user()->id)) ||
                      // ((Auth::user()->jobdesk == "3" && $r->payreq_status == 2) || (Auth::user()->jobdesk == "3" && $r->user_id == Auth::user()->id)) ||
                      // ((Auth::user()->jobdesk == "4" && $r->payreq_status == 3) || (Auth::user()->jobdesk == "4" && $r->user_id == Auth::user()->id)) ||
                      // ((Auth::user()->jobdesk == "5" && $r->payreq_status == 4) || (Auth::user()->jobdesk == "5" && $r->user_id == Auth::user()->id)) ||
                      // ((Auth::user()->jobdesk == "6" && $r->payreq_status == 5) || (Auth::user()->jobdesk == "6" && $r->user_id == Auth::user()->id)) ||
                      // ((Auth::user()->jobdesk == "7" && $r->payreq_status == 6) || (Auth::user()->jobdesk == "7" && $r->user_id == Auth::user()->id)) ||
                      // ((Auth::user()->jobdesk == "8" && $r->payreq_status == 7) || (Auth::user()->jobdesk == "8" && $r->user_id == Auth::user()->id)) ||
                      // ((Auth::user()->jobdesk == "9" && $r->payreq_status == 8) || (Auth::user()->jobdesk == "9" && $r->user_id == Auth::user()->id))
                      )
                    <tr>
                      <td>{{$r->payreq_id}}</td>
                      <td>{{$r->created_at}}</td>
                      <td>{{$r->payreq_project_code}}</td>
                      <td>{{$r->payreq_project_name}}</td>
                      <td>{{$r->payreq_type}}</td>
                      <td>{{$r->payreq_week}}</td>
                      <td>{{$r->user_name}}</td>

                      <td>Rp.{{number_format($r->payreq_total, 0, '','.')}}</td>

                      @if($r->payreq_total >= 10000000)
                            <td>
                            <?php
                              switch ($r->payreq_status) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by BOD";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                case '10':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                            </td>
                      @else
                            <td>
                            <?php
                              switch ($r->payreq_status) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                case '10':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                            </td>
                      @endif
                      
                      <td>
                        <?php
                          if (isset($r->updated_at)) {
                            $diff = date_diff(date_create($r->updated_at, timezone_open("Asia/Jakarta")), $current_date_time);
                            echo $diff->d;
                          } else {
                            echo "-";
                          }

                        ?>
                        Day(s)
                      </td>
                      <td>
                        <!-- Call to action buttons -->
                        <ul class="list-inline m-0">

                          {{-- button VIEW USER REMARK --}}
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#user_remarks_{{ $r->finance_id }}">Notes payment request</button>
                          </li>
                          <div id="user_remarks_{{ $r->finance_id }}" class="modal fade">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remarks from {{ $r->user_name }}:</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>{{ $r->user_remarks }}</p>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                    </div>
                                </div>
                            </div>
                          </div>
                          
                          {{-- button VIEW URL / FILE --}}
                          <li class="list-inline-item">
                            @if ($r->payreq_file == null)
                              <a target="_blank" href="{{url('//' . $r->payreq_url) }}">
                                <button type="button" class="btn btn-primary btn-sm">View payment request</button>
                              </a>
                            @else
                              <a target="_blank" href="{{ asset('payreqfile/' . $r->payreq_file) }}">
                                <button type="button" class="btn btn-primary btn-sm">Download payment request</button>
                              </a>
                            @endif
                          </li>

                          {{-- button INPUT PC saat status == 8 / payreq Transfered --}}
                          @if ($r->payreq_status == 9)
                            <li class="list-inline-item">
                              <a href="{{ url('pcreport/'. $r->finance_id .'/edit') }}">
                              <button type="button" class="btn btn-warning btn-sm">Input PC</button>
                              </a>
                            </li>
                          @endif

                          {{-- button REMARKS payreq --}}
                          @if ($r->payreq_status == 1 || $r->payreq_status == 0)
                            <li class="list-inline-item">
                              <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#remarks_{{ $r->finance_id }}">Remarks</button>
                            </li>

                            <div id="remarks_{{ $r->finance_id }}" class="modal fade">
                              <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h4 class="modal-title">payment request Remarks:</h4>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                      <div class="modal-body">
                                          <p>{{ $r->payreq_remarks }}</p>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          @endif

                          {{-- button EDIT, button DELETE --}}
                          @if ((($r->payreq_status == 0 || $r->payreq_status == 1) && Auth::user()->jobdesk == "1") || Auth::user()->jobdesk == "webadmin")

                            {{-- button EDIT --}}
                            <li class="list-inline-item">
                              <a href="{{ url('payreqreport/'. $r ->finance_id .'/edit') }}">
                              <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit payment request"><i class="fa fa-edit"></i></button>
                              </a>
                            </li>

                            {{-- button DELETE --}}
                            <li class="list-inline-item">
                            <form action="payreqreport/{{ $r->finance_id }}" method="POST">
                              <input type="hidden" name="_method" value="delete">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="btn btn-danger btn-sm rounded-0" type="submit" data-toggle="tooltip" data-placement="top" title="Delete payment request"><i class="fa fa-trash"></i></button>
                            </form>
                            </li>

                          @endif

                          {{-- button ACCEPT, button REJECT --}}

                          {{-- PM Reg approve & reject --}}
                          @if (Auth::user()->jobdesk == "2" && $r->payreq_status == 0)
                            <li class="list-inline-item">
                              <a href="{{ url('payreqreport/approve-0/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc payreq"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pm-reg-{{ $r->finance_id }}" data-placement="top" title="Reject Payment Request"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-pm-reg-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('payreqreport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject payment request</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- PM National approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "3" && $r->payreq_status == 2)
                            <li class="list-inline-item">
                              <a href="{{ url('payreqreport/approve-2/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc payreq"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pm-nat-{{ $r->finance_id }}" data-placement="top" title="Reject Payreq"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-pm-nat-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('payreqreport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Payment Request</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- RCM approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "4" && $r->payreq_status == 3)
                            <li class="list-inline-item">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="modal" data-target="#approve_rcm_{{ $r->finance_id }}" data-placement="top" title="Acc Payreq"><i class="fas fa-check"></i></button>

                                {{-- Modal Approval RCM --}}
                                <div id="approve_rcm_{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Approve?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('oayreqreport/approve-3/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="payreq_ahead" class="col-form-label">Nominal Payment Request Total Yang di Approve :</label>
                                            <input class="form-control" id="payreq_rcm" name="payreq_rcm" rows="1" placeholder="Hanya Inputkan Angka!" required>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-success">Approve</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-rcm-{{ $r->finance_id }}" data-placement="top" title="Reject Payreq"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-rcm-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('payreqreport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Payment Request</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- DH Approval approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "5" && $r->payreq_status == 4)
                            <li class="list-inline-item">
                              <a href="{{ url('payreqreport/approve-4/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc payreq"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-dh-{{ $r->finance_id }}" data-placement="top" title="Reject Payreq"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-dh-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('payreqreport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject payreq</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- Akuntan Approval approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "6" && $r->payreq_status == 5)
                            <li class="list-inline-item">
                              <a href="{{ url('payreqreport/approve-5/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc payreq"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-{{ $r->finance_id }}" data-placement="top" title="Reject payreq"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-akun-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('payreqreport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject payment request</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- Akuntan Head approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "7" && $r->payreq_status == 6)
                            <li class="list-inline-item">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="modal" data-target="#approve_a_head_{{ $r->finance_id }}" data-placement="top" title="Acc payreq"><i class="fas fa-check"></i></button>
                                {{-- Modal Approval Akuntan Head --}}
                                <div id="approve_a_head_{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Approve?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('payreqreport/approve-6/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="payreq_ahead" class="col-form-label">Nominal Payreq Total Yang di Approve :</label>
                                            <input class="form-control" id="payreq_ahead" name="payreq_ahead" rows="1" placeholder="Hanya Inputkan Angka!" required>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-success">Approve</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-head-{{ $r->finance_id }}" data-placement="top" title="Reject payreq"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-akun-head-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('payreqreport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject payment request</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                            {{-- <li class="list-inline-item">
                              @if ($r->multiple_payreq_on_project == 0)
                                <a href="{{ url('/payreqreport/multi/payreq/allow/' . $r->finance_id) }}" data-value-project="{{$r->finance_project_code}}" data-value-user="{{$r->user_name}}" class="allow-multiple">
                                  <button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Allow Multiple payreq"><i class="fas fa-folder-plus"></i> &nbsp; Multiple payreq</button>
                                </a>
                              @else
                                <a href="{{ url('/payreqreport/multi/payreq/revoke/' . $r->finance_id) }}" data-value-project="{{$r->finance_project_code}}" data-value-user="{{$r->user_name}}" class="revoke-multiple">
                                  <button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Revoke Multiple payreq Permission"><i class="fas fa-folder-minus"></i> &nbsp; Revoke</button>
                                </a>
                              @endif
                            </li> --}}

                          {{--BOD approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "8" && $r->payreq_status == 7)
                            <li class="list-inline-item">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="modal" data-target="#approve_bod_{{ $r->finance_id }}" data-placement="top" title="Acc payreq"><i class="fas fa-check"></i></button>
                                {{-- Modal Approval BOD --}}
                                <div id="approve_bod_{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Approve?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('payreqreport/approve-7/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="payreq_ahead" class="col-form-label">Nominal payreq Total Yang di Approve :</label>
                                            <input class="form-control" id="payreq_bod" name="payreq_bod" rows="1" placeholder="Hanya Inputkan Angka!" required>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-success">Approve</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>
                            <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-bod-{{ $r->finance_id }}" data-placement="top" title="Reject payreq"><i class="fas fa-window-close"></i></button>

                                <div id="reject-bod-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('payreqreport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject payment request</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>

                          {{-- Akuntan Transfer approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "9" && $r->payreq_status == 8)
                            <li class="list-inline-item">
                              <a href="{{ url('payreqreport/approve-8/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc payreq"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-tf-{{ $r->finance_id }}" data-placement="top" title="Reject payreq"><i class="fas fa-window-close"></i></button>

                                <div id="reject-akun-tf-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('payreqreport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject Payment Request</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>
                          @endif
                        </ul>
                      </td>
                    </tr>
                  @endif
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                      <th></th>
                      <th>Date Created</th>
                      <th>Project Code</th>
                      <th>Project Name</th>
                      <th>Week payment request</th>
                      <th>Submitted By</th>
                      <th>Total payment request</th>
                  </tr>
                </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    @endif
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->
<!-- REQUIRED SCRIPTS -->
<!-- Sweet ALert -->
<link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Sweet Alert -->
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
  $(document).ready(function() {
    var table = $("#payreq-table").DataTable({
      // responsive: true, 
      lengthChange: false, 
      autoWidth:false,
      buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
      dom: 'Bfrtip',
    });

    $('.allow-multiple').click(function(e) {
        e.preventDefault();
        const HREF = $(this).attr('href');
        const USER_NAME = $(this).attr('data-value-user');
        const PROJECT_CODE = $(this).attr('data-value-project');
        Swal.fire({
                icon: 'warning',
                title: 'Apakah Anda yakin?',
                text: 'Dengan menekan YA, maka user ('+ USER_NAME + ') akan dapat memasukkan payreq lain untuk project (' + PROJECT_CODE + ')',
                showCancelButton: true,
                confirmButtonText: '  YA  ',
                cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                document.location.href = HREF;
            }
        });
        });

    $('.revoke-multiple').click(function(e) {
        e.preventDefault();
        const HREF = $(this).attr('href');
        const USER_NAME = $(this).attr('data-value-user');
        const PROJECT_CODE = $(this).attr('data-value-project');
        Swal.fire({
                icon: 'warning',
                title: 'Apakah Anda yakin?',
                text: 'Dengan menekan YA, maka user ('+ USER_NAME + ') tidak akan dapat memasukkan payreq lain untuk project (' + PROJECT_CODE + ')',
                showCancelButton: true,
                confirmButtonText: '  YA  ',
                cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                document.location.href = HREF;
            }
        });
        });

    $('#payreq-table tfoot th').each( function (i) {
        var title = $('#payreq-table thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" data-index="'+i+'" />' );
    } );
  
    $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
      table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );
    
  });

</script>
@endsection
