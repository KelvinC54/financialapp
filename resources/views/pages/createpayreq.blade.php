@extends('layouts.main')

@section('title','Input Payment Request')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Input Payment Request</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Payment Request</a></li>
              <li class="breadcrumb-item active">Input</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->

            <!-- /.card -->

              <div>
              <div class="card-body" id="settings">
                <form class="form-horizontal" action="{{ route('payreqreport.store') }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  
                  <!--<div class="form-group row">-->
                  <!--  <label class="col-sm-2 col-form-label">Project Code</label>-->
                  <!--  <div class="col-md-6">-->
                  <!--      <input type="text" class="form-control " id="finance_project_code" name="finance_project_code" placeholder="">-->
                  <!--      <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="right" onclick="myFunction()"  title="Opsi"> <i class="fas fa-chevron-circle-down"></i></button>-->
                  <!--      <div id="myDropdown" class="dropdown-content">-->
                  <!--        <input type="text" class="form-control" placeholder="Search.." id="myInput" onkeyup="filterFunction()">-->
                  <!--        @foreach ($projects as $p)-->
                  <!--          <a href="#{{$p->project_code}}" data-value-code="{{$p->project_code}}" data-value-name="{{$p->project_name}}">-->
                  <!--            {{$p->project_code}} &nbsp; - &nbsp; {{$p->project_name}}-->
                  <!--          </a>-->
                  <!--        @endforeach-->
                  <!--  </div>-->
                  <!-- </div>-->
                  <!--</div>-->
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Code</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="payreq_project_code" name="payreq_project_code" placeholder="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="payreq_project_name" name="payreq_project_name" placeholder="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Week Payment Request</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="payreq_week" name="payreq_week" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_id" class="col-sm-2 col-form-label">Submitted by NIP | Name</label>
                    <div class="col-sm-10">
                        <input id="user_id" type="text" name="user_id" value="{{ Auth::user()->nip }}" readonly>
                        <input id="user_name" type="text" name="user_name" value="{{ Auth::user()->name }}" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_designation"class="col-sm-2 col-form-label">Designation</label>
                    <div class="col-sm-10">
                      <input id="user_designation" type="text" name="user_designation" value="{{ Auth::user()->designation }}" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Total Payment Request</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="payreq_total" name="payreq_total" placeholder="ex: 1000000 (Hanya angka tanpa titik dan Rupiah)">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pilih Cara Upload File Payment Request</label>
                    <div class="col-sm-10">
                      <label class="col-form-label pb-3">
                        <input class="upload_option" type="radio" name="upload_option" value="URL" checked> URL
                      </label>
                      <br>
                      <label class="col-form-label pb-3">
                        <input class="upload_option" type="radio"name="upload_option" value="FILE"> Upload File
                      </label>
                    </div>
                  </div>

                  <div class="form-group row use-file" style="display:none;">
                    <label class="col-sm-2 col-form-label">File Payment Request</label>
                    <div class="col-sm-10">
                        <input type="file" name="reim_file">
                    </div>
                  </div>

                  <div class="form-group row use-url">
                    <label class="col-sm-2 col-form-label">URL File Payment Request</label>
                    <div class="col-sm-10">
                      <input id="reim_url" type="text" name="reim_url" placeholder="www.url.com/subdomain" />
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- Sweet ALert -->
<link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Sweet Alert -->
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script>
    $("input[type='file']").on("change", function() {
        if (this.files[0].size > 10000000) {
          Swal.fire({
                      icon: 'error',
                      title: 'Ukuran File Terlalu Besar',
                      text: 'Silakan upload file dengan ukuran kurang dari 10MB',
              });
          this.value = '';
          return false;
        } else {
          var pathFile = this.value;
          var ekstensiOk = /(\.jpg|\.jpeg|\.png|\.doc|\.docx|\.xlsx|\.xls|\.txt|\.pdf|\.zip|\.csv)$/i;
          if(!ekstensiOk.exec(pathFile)){
              Swal.fire({
                      icon: 'error',
                      title: 'Ekstensi File Salah!',
                      text: 'Silakan upload file dengan ekstensi jpg,jpeg,png,doc,docx,xlsx,xls,txt,pdf,zip',
              });
              this.value = '';
              return false;
          }else{
            // create CA tanpa ajax
            Swal.fire({
                    icon:'success',
                    title: 'File Benar!',
                    text: 'Ukuran dan ekstensi file sudah tepat',
                    showConfirmButton: true,
              });
            
            // create CA with ajax
            // var ekstensiPdf = /(\.pdf)$/i;
            // if (ekstensiOk.exec(pathFile)) {
            //   uploadFileLive('pdf');
            // } else {
            //   uploadFileLive('others');
            // }          
          }
        }
    });

      
    $('.upload_option').on('click', function() {
        var upload_option = $('input[name="upload_option"]:checked').val();
        if (upload_option == 'URL') {
            $('.use-url').show();
            $('.use-file').hide();
        } else if (upload_option == 'FILE'){
            $('.use-url').hide();
            $('.use-file').show();
        }
    });
    
    function myFunction() {
      document.getElementById("myDropdown").classList.toggle("show");
    }

    function filterFunction() {
      var input, filter, ul, li, a, i;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      div = document.getElementById("myDropdown");
      a = div.getElementsByTagName("a");
      for (i = 0; i < a.length; i++) {
        txtValue = a[i].textContent || a[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          a[i].style.display = "";
        } else {
          a[i].style.display = "none";
        }
      }
    }

    $('#myDropdown a').on('click', function() {
      $('#finance_project_code').val($(this).attr('data-value-code'));
      $('#finance_project_name').val($(this).attr('data-value-name'));
    });
    
   
</script>
@endsection
