@extends('layouts.main')

@section('title','Approval')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Petty Cash Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Petty Cash</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->

              <!-- /.card -->

              <div class="card">
                <div class="card-header col-md-12">
                  <!-- <button col-md-12 type="button" class="btn btn-warning">Add New CA</button></li>-->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="pc" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Date Created</th>
                      <th>Project Code</th>
                      <th>Project Name</th>
                      <th>Week CA</th>
                      <th>Submited By</th>
                      <th>Total CA</th>
                      <th>Total PC</th>
                      <th>Sisa CA</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>


                    <tbody>

                    <tr>
                      <td>001</td>
                      <td>12-12-2021</td>
                      <td>068 VLTI-KAL-TSEL-FMC-2020</td>
                      <td>Telkominfra Tsel Full Maintenance</td>
                      <td>1</td>
                      <td>Fatmawaty. BN</td>
                      <td>10.000.000</td>
                      <td>7.000.000</td>
                      <td>3.000.000</td>
                      <td>Rejected</td>
                      <td>
                        <!-- Call to action buttons -->
                        <ul class="list-inline m-0">
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-primary">View CA</button></li>
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-warning">View PC</button></li>
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-secondary">Remarks</button></li>
                          <li class="list-inline-item">
                            <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit CA"><i class="fa fa-edit"></i></button>
                          </li>
                          <li class="list-inline-item">
                            <button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete CA"><i class="fa fa-trash"></i></button>
                          </li>
                          <li class="list-inline-item">
                            <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                          </li>
                          <li class="list-inline-item">
                            <button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>
                          </li>
                        </ul>
                      </td>

                    </tr>


                    <tr>
                      <td>002</td>
                      <td>11-12-2021</td>
                      <td>069 huawei</td>
                      <td>Telkominfra Tsel Full Maintenance</td>
                      <td>2</td>
                      <td>Fatmawaty. BN</td>
                      <td>20.000.000</td>
                      <td>17.000.000</td>
                      <td>3.000.000</td>
                      <td>Rejected</td>
                      <td>
                        <!-- Call to action buttons -->
                        <ul class="list-inline m-0">
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-primary">View CA</button></li>
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-warning">View PC</button></li>
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-secondary">Remarks</button></li>
                          <li class="list-inline-item">
                            <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit CA"><i class="fa fa-edit"></i></button>
                          </li>
                          <li class="list-inline-item">
                            <button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete CA"><i class="fa fa-trash"></i></button>
                          </li>
                          <li class="list-inline-item">
                            <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                          </li>
                          <li class="list-inline-item">
                            <button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>
                          </li>
                        </ul>
                      </td>
                    </tr>


                    <tr>
                      <td>003</td>
                      <td>10-12-2021</td>
                      <td>070 zte</td>
                      <td>Telkominfra Tsel Full Maintenance</td>
                      <td>3</td>
                      <td>Fatmawaty. BN</td>
                      <td>30.000.000</td>
                      <td>27.000.000</td>
                      <td>3.000.000</td>
                      <td>Rejected</td>
                      <td>
                        <!-- Call to action buttons -->
                        <ul class="list-inline m-0">
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-primary">View CA</button></li>
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-warning">View PC</button></li>
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-secondary">Remarks</button></li>
                          <li class="list-inline-item">
                            <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit CA"><i class="fa fa-edit"></i></button>
                          </li>
                          <li class="list-inline-item">
                            <button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete CA"><i class="fa fa-trash"></i></button>
                          </li>
                          <li class="list-inline-item">
                            <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                          </li>
                          <li class="list-inline-item">
                            <button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>
                          </li>
                        </ul>
                      </td>
                    </tr>



                    </tbody>

                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right
    <div class="float-right d-none d-sm-inline">
      Anything you want
    -->
    <!-- Default to the left -->
    <strong>Copyright &copy; 2021 >Velacom</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection