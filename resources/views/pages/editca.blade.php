@extends('layouts.main')

@section('title','createca')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit Cash Advance</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Cash Advance</a></li>
              <li class="breadcrumb-item active">Input</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              @if(Session::has('update_success'))
              <div class="alert alert-success">
                  {{Session::get('update_success')}}
              </div>
              @endif
              <!-- /.card-header -->

            <!-- /.card -->

              <!-- <div class="card">
                <div class="card-header col-md-12">
                  <button col-md-12 type="button" class="btn btn-outline-primary">Add New CA</button></li>
                </div>-->
              <!-- /.card-header -->
              <div>
              <div class="card-body" id="settings">
                <form class="form-horizontal" action="/careport/update/{{ $finance_reports->finance_id }}" method="POST">
                  @csrf
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Code</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="finance_project_code" name="finance_project_code" value="{{ $finance_reports->finance_project_code }}" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="finance_project_name" name="finance_project_name" value="{{ $finance_reports->finance_project_name }}" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Type CA</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="ca_type" name="ca_type" value="{{ $finance_reports->ca_type }}" placeholder="OPS, BBM, etc..">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Week CA</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="finance_week" name="finance_week" value="{{ $finance_reports->finance_week }}" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_id" class="col-sm-2 col-form-label">Submitted by NIP | Name</label>
                    <div class="col-sm-10">
                        <input id="user_id" type="text" placeholder="user_id" name="user_id" value="{{ Auth::user()->nip }}" disabled>
                        <input id="user_name" type="text" placeholder="user_name" name="user_name" value="{{ Auth::user()->name }}" disabled>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_designation"class="col-sm-2 col-form-label">Designation</label>
                    <div class="col-sm-10">
                      <input id="user_designation" type="text" placeholder="user_designation" name="user_designation" value="{{ Auth::user()->designation }}" disabled>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Total Cash Advance</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="ca_total" name="ca_total" value="{{ $finance_reports->ca_total }}" placeholder="ex: 1000000">
                    </div>
                  </div>
                  <div class="form-group row">
                    {{-- <label class="col-sm-2 col-form-label">File CA</label>
                    <div class="col-sm-10">
                        <input type="file" accept="ca_file/*" name="ca_file">
                    </div> --}}
                    <label class="col-sm-2 col-form-label">URL File CA</label>
                    <div class="col-sm-10">
                      <input id="ca_url" type="text" name="ca_url" value="{{ $finance_reports->ca_url }}" placeholder="" />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Old Remark From Creator</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" type="text" id="doc_remarks" name="doc_remarks" rows="10" value="{{ $finance_reports->user_remarks }}" placeholder=""></textarea>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">New Remark From Creator</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" type="text" id="doc_remarks" name="doc_remarks" rows="10" value="" placeholder=""></textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script>
</script>
@endsection
