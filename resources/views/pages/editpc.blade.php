@extends('layouts.main')

@section('title','createpc')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Input Petty Cash</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Petty Cash</a></li>
              <li class="breadcrumb-item active">Input</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div>

                  <div class="card-body" id="settings">
                    <form action="/pcreport/{{ $finance_reports->finance_id }}" method="POST" enctype="multipart/form-data" class="form-horizontal">

                      <!--<div class="form-group row">
                        <label>Project Code</label>
                        <input id="Project Code" type="text" placeholder="Project Code" name="Project Code" value="Project Code" autocomplete="Project Code" disabled>
                        </div>-->

                      <div class="form-group row">
                        <label for="Project Code" class="col-sm-2 col-form-label">Project Code</label>
                        <div class="col-sm-10">
                          <input id="Project Code" type="text" placeholder="Project Code" name="Project Code" value="{{ $finance_reports->finance_project_code }}" autocomplete="Project Code" disabled>
                        </div>
                      </div>

                        <div class="form-group row">
                          <label for="Project Name" class="col-sm-2 col-form-label">Project Name</label>
                          <div class="col-sm-10">
                            <input id="Project Name" type="text" placeholder="Project Name" name="Project Name" value="{{ $finance_reports->finance_project_name }}" autocomplete="Project Name" disabled>
                          </div>
                        </div>

                      <div class="form-group row">
                        <label for="Week CA" class="col-sm-2 col-form-label">Week CA</label>
                        <div class="col-sm-10">
                          <input id="Week CA" type="text" placeholder="Week CA" name="Week CA" value="{{ $finance_reports->finance_week }}" autocomplete="Week CA" disabled>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="Date CA" class="col-sm-2 col-form-label">Date CA</label>
                        <div class="col-sm-10">
                          <input id="Date CA" type="text" placeholder="Date CA" name="Date CA" value="{{ $finance_reports->created_at }}" autocomplete="Date CA" disabled>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="Submitted" class="col-sm-2 col-form-label">Submitted by</label>
                        <div class="col-sm-10">
                          <input id="Submitted" type="text" placeholder="Submitted" name="Submitted" value="{{ $finance_reports->user_name }}" autocomplete="Submitted" disabled>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="Designation" class="col-sm-2 col-form-label">Designation</label>
                        <div class="col-sm-10">
                          <input id="Designation" type="text" placeholder="Designation" name="Designation" value="{{ $finance_reports->user_designation }}" autocomplete="Designation" disabled>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="Total Cash Advance" class="col-sm-2 col-form-label">Total Cash Advance</label>
                        <div class="col-sm-10">
                          {{-- view CA Total --}}
                          @if ($r->ca_rcm == null && $r->ca_ahead == null && $r->ca_bod == null)
                            <input id="Total Cash Advance" type="text" placeholder="Total Cash Advance" name="Total Cash Advance"
                          value="Rp.{{number_format($report->ca_total, 0, '','.')}}" autocomplete="Total Cash Advance" disabled>

                          {{-- view CA RCM --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead == null && $r->ca_bod == null)
                            <input id="Total Cash Advance" type="text" placeholder="Total Cash Advance" name="Total Cash Advance"
                          value="Rp.{{number_format($report->ca_rcm, 0, '','.')}}" autocomplete="Total Cash Advance" disabled>

                          {{-- view CA A Head --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod == null)
                            <input id="Total Cash Advance" type="text" placeholder="Total Cash Advance" name="Total Cash Advance"
                          value="Rp.{{number_format($report->ca_ahead, 0, '','.')}}" autocomplete="Total Cash Advance" disabled>

                          {{-- view CA BOD --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod != null)
                            <input id="Total Cash Advance" type="text" placeholder="Total Cash Advance" name="Total Cash Advance"
                          value="Rp.{{number_format($report->ca_bod, 0, '','.')}}" autocomplete="Total Cash Advance" disabled>
                          @endif
                        </div>
                      </div>

                        <div class="form-group row">
                          <label for="inputSkills" class="col-sm-2 col-form-label">Total Petty Cash</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputSkills" name="pc_total" placeholder="Masukkan Total PC">
                          </div>
                        </div>

                        {{-- <div class="form-group row">
                          <label for="inputSkills" class="col-sm-2 col-form-label">Total Balance</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputSkills" placeholder="Masukkan selisih dari CA & PC">
                          </div>
                        </div> --}}

                        <div class="form-group row">
                          <label for="inputSkills" class="col-sm-2 col-form-label">File PC</label>
                          <div class="col-sm-10">
                            <input type="file" name="file_attach[]" multiple/>
                          </div>

                          <label for="inputSkills" class="col-sm-2 col-form-label">URL File PC</label>
                          <div class="col-sm-10">
                            <input type="url" name="url" placeholder="www.xxx.com/xx" />
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="inputSkills" class="col-sm-2 col-form-label">Nota transfer Sisa CA</label>
                          <div class="col-sm-10">
                            <input type="file" name="file_attach[]" multiple/>
                          </div>
                          <label for="inputSkills" class="col-sm-2 col-form-label">URL Nota Transfer</label>
                          <div class="col-sm-10">
                            <input type="url" name="url" placeholder="www.xxx.com/xx" />
                          </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Remark PC From Creator</label>
                            <div class="col-sm-10">
                              <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="10" placeholder="Inputkan Catatan untuk PC yang diajukan" required></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="offset-sm-2 col-sm-10">
                            <button type="submit" class="btn btn-danger">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script>
</script>
@endsection
