@extends('layouts.main')

@section('title','Reimbursement Report')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Reimbursement Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Reimbursement</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->

            <!-- /.card -->

            <div class="card">
              <div class="card-header col-md-12">
                @if(Session::has('delete_success'))
                <div class="alert alert-warning">
                    {{Session::get('delete_success')}}
                </div>
                @endif
                @if(Session::has('reject_success'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  {{Session::get('reject_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <a href="{{ route('reimbursereport.create') }}">
                  <button col-md-12 type="button" class="btn btn-outline-primary">Reimburse</button>
                </a>
                <br>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-2">
                <table id="reimburse-table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Date Created</th>
                    <th>Project Code</th>
                    <th>Project Name</th>
                    <th>Week Reimbursement</th>
                    <th>Submited By</th>
                    <th>Total Reimbursement</th>
                    <th>Status</th>
                    <th>Aging Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($reports as $r)
                    @if (
                      (Auth::user()->jobdesk == "webadmin" && $r->status_reimbursement >= 0) ||
                      // ((Auth::user()->jobdesk == "1" && $r->user_id == Auth::user()->id) && $r->status_reimbursement < 10) ||
                      (Auth::user()->jobdesk == "1" && $r->status_reimbursement < 10) ||
                      (Auth::user()->jobdesk == "2" && $r->status_reimbursement >= 0) ||
                      ((Auth::user()->jobdesk == "3" && $r->status_reimbursement == 2) || (Auth::user()->jobdesk == "3" && Auth::user()->nip == $r->user_id)) ||
                      (Auth::user()->jobdesk == "4" && $r->status_reimbursement == 3) ||
                      (Auth::user()->jobdesk == "5" && $r->status_reimbursement == 4) ||
                      (Auth::user()->jobdesk == "6" && $r->status_reimbursement >= 0) ||
                      (Auth::user()->jobdesk == "7" && $r->status_reimbursement == 6) ||
                      (Auth::user()->jobdesk == "7" && $r->status_reimbursement == 7) ||
                      (((Auth::user()->jobdesk == "9" && $r->user_id == Auth::user()->nip) && $r->status_reimbursement != 9) ||
                      (Auth::user()->jobdesk == "9" && $r->status_reimbursement == 8))
                      
                    )
                    <tr>
                      <td>{{$r->id}}</td>
                      <td>{{$r->created_at}}</td>
                      <td>{{$r->project_code}}</td>
                      <td>{{$r->project_name}}</td>
                      <td>{{$r->week_reimbursement}}</td>
                      <td>{{$r->user_name}}</td>
                      <td>Rp.{{number_format($r->total_reimbursement, 0, '','.')}}</td>
                      <td>
                        @if($r->ca_total >= 10000000)
                            <?php
                              switch ($r->status_reimbursement) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by BOD";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                        @else
                            <?php
                              switch ($r->status_reimbursement) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                        @endif
                      </td>
                      <td>
                        <?php
                          if (isset($r->updated_at)) {
                            $diff = date_diff(date_create($r->updated_at, timezone_open("Asia/Jakarta")), $current_date_time);
                            echo $diff->d;
                          } else {
                            echo "-";
                          }
                          
                        ?>
                        
                        Day(s)
                      </td>
                      <td>
                        <!-- Call to action buttons -->
                        <ul class="list-inline m-0">
                            
                            {{-- button VIEW USER REMARK --}}
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#user_remarks_{{ $r->id }}">Notes</button>
                          </li>
                          <div id="user_remarks_{{ $r->id }}" class="modal fade">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remarks from {{ $r->user_name }}:</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>{{ $r->user_remarks }}</p>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                    </div>
                                </div>
                            </div>
                          </div>
                          
                          {{-- button VIEW URL / FILE --}}
                          <li class="list-inline-item">
                            @if ($r->url_reimbursement != null)
                              <!--<a target="_blank" href="{{url('//' . $r->url_reimbursement) }}">-->
                              <a target="_blank" href="{{$r->url_reimbursement }}">
                                <button type="button" class="btn btn-sm btn-primary">View File</button>
                              </a>
                            @else
                              <a target="_blank" href="{{ asset('reimfile/' . $r->file_reimbursement) }}">
                                <button type="button" class="btn btn-sm btn-primary">Download File</button>
                              </a>
                            @endif
                          </li>

                          {{-- button REMARKS CA --}}
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-outline-secondary">Remarks</button>
                          </li>

                          {{-- button EDIT, button DELETE --}}
                          @if ((($r->status_reimbursement == 0 || $r->status_reimbursement == 1) && Auth::user()->jobdesk == "1") || Auth::user()->jobdesk == "webadmin")
                            {{-- button EDIT --}}
                            <li class="list-inline-item">
                              <a href="{{ url('reimbursereport/'. $r ->id .'/edit') }}">
                              <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit CA"><i class="fa fa-edit"></i></button>
                              </a>
                            </li>
                            {{-- button DELETE --}}
                            <li class="list-inline-item">
                              <a href="/reimbursereport/delete/{{ $r->id }}" class="hapus-report">
                                <button class="btn btn-danger btn-sm rounded-0" type="submit" data-toggle="tooltip" data-placement="top" title="Delete CA"><i class="fa fa-trash"></i></button>
                              </a>
                            </li>
                          @endif

                          {{-- button ACCEPT, button REJECT --}}

                          {{-- PM Reg approve & reject --}}
                          @if (Auth::user()->jobdesk == "2" && $r->status_reimbursement == 0)
                            <li class="list-inline-item">
                              <a href="{{ url('reimbursereport/approve-0/' . $r->id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pm-valid-{{ $r->id }}" data-placement="top" title="Reject Reimbursement"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-pm-valid-{{ $r->id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('reimbursereport/reject/' . $r->id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Reimbursement</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- PM National approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "3" && $r->status_reimbursement == 2)
                            <li class="list-inline-item">
                              <a href="{{ url('reimbursereport/approve-2/' . $r->id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pm-national-{{ $r->id }}" data-placement="top" title="Reject Reimbursement"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-pm-national-{{ $r->id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('reimbursereport/reject/' . $r->id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Reimbursement</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- RCM approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "4" && $r->status_reimbursement == 3)
                            <li class="list-inline-item">
                              <a href="{{ url('reimbursereport/approve-3/' . $r->id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pcm-valid-{{ $r->id }}" data-placement="top" title="Reject Reimbursement"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-pcm-valid-{{ $r->id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('reimbursereport/reject/' . $r->id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Reimbursement</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                          
                          {{-- DH approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "5" && $r->status_reimbursement == 4)
                            <li class="list-inline-item">
                              <a href="{{ url('reimbursereport/approve-4/' . $r->id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pd-{{ $r->id }}" data-placement="top" title="Reject Reimbursement"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-pd-{{ $r->id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('reimbursereport/reject/' . $r->id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Reimbursement</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                          
                          {{-- Akuntan Approval approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "6" && $r->status_reimbursement == 5)
                            <li class="list-inline-item">
                              <a href="{{ url('reimbursereport/approve-5/' . $r->id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-{{ $r->id }}" data-placement="top" title="Reject Reimbursement"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-akun-{{ $r->id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('reimbursereport/reject/' . $r->id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Reimbursement</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                          
                          {{-- Akuntan Head approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "7" && $r->status_reimbursement == 6)
                            <li class="list-inline-item">
                              <a href="{{ url('reimbursereport/approve-6/' . $r->id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-head-{{ $r->id }}" data-placement="top" title="Reject Reimbursement"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-akun-head-{{ $r->id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('reimbursereport/reject/' . $r->id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Reimbursement</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                            
                          {{-- Akuntan Head approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "8" && $r->status_reimbursement == 7)
                            <li class="list-inline-item">
                              <a href="{{ url('reimbursereport/approve-7/' . $r->id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc Reimbursement"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-head-{{ $r->id }}" data-placement="top" title="Reject Reimbursement"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-akun-head-{{ $r->id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('reimbursereport/reject/' . $r->id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Reimbursement</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                          
                          {{-- Akuntan Transfer approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "9" && $r->status_reimbursement == 8)
                            <li class="list-inline-item">
                              <a href="{{ url('reimbursereport/approve-8/' . $r->id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-tf-{{ $r->id }}" data-placement="top" title="Reject Reimbursement"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-akun-tf-{{ $r->id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('reimbursereport/reject/' . $r->id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject Reimbursement</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                          @endif
                        </ul>
                      </td>
                    </tr>
                    @endif
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                        <th></th>
                        <th>Date Created</th>
                        <th>Project Code</th>
                        <th>Project Name</th>
                        <th>Week Reimbursement</th>
                        <th>Submitted By</th>
                        <th>Total Reimbursement</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->
<!-- REQUIRED SCRIPTS -->
<!-- Sweet ALert -->
<link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Sweet Alert -->
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
  var table = $("#reimburse-table").DataTable({
    // responsive: true, 
    lengthChange: false, 
    autoWidth:false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    dom: 'Bfrtip',
  });

  $('.hapus-report').click(function(e) {
      e.preventDefault();
      const HREF = $(this).attr('href');
      Swal.fire({
              icon: 'warning',
              title: 'Apakah Anda yakin?',
              text: 'Jika report dihapus, maka tidak bisa dikembalikan lagi',
              showCancelButton: true,
              confirmButtonText: '  YA  ',
              cancelButtonColor: '#d33',
      }).then((result) => {
          if (result.value) {
              document.location.href = HREF;
          }
      });
      });

  $('#reimburse-table tfoot th').each( function (i) {
      var title = $('#reimburse-table thead th').eq( $(this).index() ).text();
      $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" data-index="'+i+'" />' );
  } );

  $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
    table
          .column( $(this).data('index') )
          .search( this.value )
          .draw();
  } );
</script>
@endsection
