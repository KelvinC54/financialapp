@extends('layouts.main')

@section('title','Edit Reimbursement')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Input Reimbursement</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Reimbursement</a></li>
              <li class="breadcrumb-item active">Input</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->

            <!-- /.card -->
            
              <div>
              <div class="card-body" id="settings">
                <form method="POST" class="form-horizontal" action="/reimbursereport/update/{{ $report->id }}" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Code</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="reim_project_code" name="reim_project_code" placeholder="" value="{{$report->project_code}}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="reim_project_name" name="reim_project_name" placeholder="" value="{{$report->project_name}}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Week Reimbursement</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="reim_week" name="reim_week" placeholder="" value="{{$report->week_reimbursement}}">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_id" class="col-sm-2 col-form-label">Submitted by NIP | Name</label>
                    <div class="col-sm-10">
                        <input id="user_id" type="text" name="user_id" value="{{ Auth::user()->nip }}" readonly>
                        <input id="user_name" type="text" name="user_name" value="{{ Auth::user()->name }}" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_designation"class="col-sm-2 col-form-label">Designation</label>
                    <div class="col-sm-10">
                      <input id="user_designation" type="text" name="user_designation" value="{{ Auth::user()->designation }}" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Total Reimbursement</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="reim_total" name="reim_total" placeholder="ex: 1000000" value="{{$report->total_reimbursement}}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">File Reimbursement</label>
                    <div class="col-sm-10">
                        <input type="file" name="reim_file">
                        <br>
                        @if (isset($report->file_reimbursement))
                        <small>File Sekarang: {{$report->file_reimbursement}}</small>
                        @else
                        <small>File Sekarang: - </small>
                        @endif
                    </div>
                    <label class="col-sm-2 col-form-label">URL File Reimbursement</label>
                    <div class="col-sm-10">
                      <input id="reim_url" type="text" name="reim_url" placeholder="www.url.com/subdomain" value="{{$report->url_reimbursement}}"/>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Remark Reimburse From Creator</label>
                            <div class="col-sm-10">
                              <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="10" placeholder="Inputkan Catatan untuk Reimburse yang diajukan" required></textarea>
                            </div>
                  </div>
                  
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- Sweet ALert -->
<link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Sweet Alert -->
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
    $("input[type='file']").on("change", function() {
        if (this.files[0].size > 10000000) {
          Swal.fire({
                      icon: 'error',
                      title: 'Ukuran File Terlalu Besar',
                      text: 'Silakan upload file dengan ukuran kurang dari 10MB',
              });
          this.value = '';
          return false;
        } else {
          var pathFile = this.value;
          var ekstensiOk = /(\.jpg|\.jpeg|\.png|\.doc|\.docx|\.xlsx|\.xls|\.txt|\.pdf|\.zip|\.csv)$/i;
          if(!ekstensiOk.exec(pathFile)){
              Swal.fire({
                      icon: 'error',
                      title: 'Ekstensi File Salah!',
                      text: 'Silakan upload file dengan ekstensi jpg,jpeg,png,doc,docx,xlsx,xls,txt,pdf,zip',
              });
              this.value = '';
              return false;
          }else{
            // create CA tanpa ajax
            Swal.fire({
                    icon:'success',
                    title: 'File Benar!',
                    text: 'Ukuran dan ekstensi file sudah tepat',
                    showConfirmButton: true,
              });
            
            // create CA with ajax
            // var ekstensiPdf = /(\.pdf)$/i;
            // if (ekstensiOk.exec(pathFile)) {
            //   uploadFileLive('pdf');
            // } else {
            //   uploadFileLive('others');
            // }          
          }
        }
    });
</script>
@endsection
