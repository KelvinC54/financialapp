@extends('layouts.main')

@section('title','Data User')

@section('main-content')

<style type="text/css">
.step {
	display: none;
}
.step.active {
	display: block;
}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Data User</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <!-- /.card -->
                <div class="row">
                <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                        <a href="/user/create">
                            <button class="btn btn-success"><b>Tambah User</b></button>
                        </a>
                        <br>
                        @if(Session::has('delete_success'))
                        <div class="alert alert-warning">
                            {{Session::get('delete_success')}}
                        </div>
                        @endif
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="card-body table-responsive p-0">
                        <table id="dashboard-table" class="table table-hover text-nowrap">
                            <thead>
                            <tr>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Jobdesk</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{$user->nip}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->real_password}}</td>
                                <td>{{$user->designation}}</td>
                                <td>
                                    <div class="row text-left">
                                        <div class="col">
                                            <a href="/user/{{$user->id}}/edit">
                                                <button class="btn btn-sm btn-block btn-warning">Edit</button> <br>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a href="/user/delete/{{$user->id}}" class="hapus-user">
                                                <button class="btn btn-sm btn-block btn-danger">Hapus</button> <br>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- Sweet ALert -->
<link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Sweet Alert -->
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>


<script>
    $("#dashboard-table").DataTable({
    responsive: true, 
    lengthChange: false, 
    autoWidth:false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    dom: 'Bfrtip',
    });

    $('.hapus-user').click(function(e) {
        e.preventDefault();
        const HREF = $(this).attr('href');
        Swal.fire({
                icon: 'warning',
                title: 'Apakah Anda yakin?',
                text: 'Jika user dihapus, maka tidak bisa dikembalikan lagi',
                showCancelButton: true,
                confirmButtonText: '  YA  ',
                cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                document.location.href = HREF;
            }
        });
        });
</script>
@endsection
