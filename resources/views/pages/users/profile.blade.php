@extends('layouts.main')

@section('title','Profile User')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Profile User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">User</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <!-- cek alert ubah password -->
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @elseif(Session::has('update_password_success'))
                <div class="alert alert-success">
                    {{Session::get('update_password_success')}}
                </div>
                @endif

            <!-- /.card -->
              <div>
              <div class="card-body" id="settings">
                <form class="form-horizontal" action="/user/update/password/{{ Auth::user()->id }}" method="POST">
                  @csrf
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">NIP</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nip" name="nip" value="{{ Auth::user()->nip }}" placeholder="" disabled>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="name" name="name" value="{{ Auth::user()->name }}" placeholder="" disabled>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}" placeholder="" disabled>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_designation"class="col-sm-2 col-form-label">Jobdesk</label>
                    <div class="col-sm-10">
                      <select name="kode_jobdesk" class="form-control"
                      id="inlineFormCustomSelectPref" required disabled>
                        @foreach ($jobdesk as $j)
                            @if($j->kode_jobdesk == Auth::user()->jobdesk)
                            <option value="{{$j->kode_jobdesk}}" selected>{{$j->nama_jobdesk}}</option>
                            @else
                            <option value="{{$j->kode_jobdesk}}">{{$j->nama_jobdesk}}</option>
                            @endif
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Password Baru</label>
                    <div class="col-sm-10">
                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Konfirmasi Password Baru</label>
                    <div class="col-sm-10">
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-success">Update Password</button>
                    </div>
                  </div>
                </form>
              </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script>
</script>
@endsection
