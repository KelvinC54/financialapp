@extends('layouts.main')

@section('title','Approval')

@section('main-content')

<style type="text/css">
.step {
	display: none;
}
.step.active {
	display: block;
}
</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            @if ($finance_type == 'ca')
              <h1 class="m-0">CA Rejections Reports</h1>
            @elseif ($finance_type == 'pc')
              <h1 class="m-0">PC Rejections Reports</h1>
            @elseif ($finance_type == 'rm')
              <h1 class="m-0">Reimbursement Rejections Reports</h1>
            @endif
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Approval</a></li>
              <li class="breadcrumb-item active">CA</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <!-- /.card -->
              <div class="card">
                <div class="card-header col-md-12">
                  <!-- <button col-md-12 type="button" class="btn btn-warning">Add New CA</button></li>-->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="projects-table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Project Code</th>
                      <th>Project Name</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach ($projects as $project)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$project->project_code}}</td>
                        <td>{{$project->project_name}}</td>
                        <td><button class="btn btn-warning text-center" type="button" data-toggle="modal" data-target="#rejection-project-{{$loop->iteration}}" data-placement="top"><b>DETAIL</b></button>
                          <div id="rejection-project-{{$loop->iteration}}" class="modal fade">
                            <div class="modal-dialog modal-dialog-scrollable modal-xl">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Project "{{$project->project_code}}" Rejections</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <table class="rejection-table table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Week </th>
                                      <th>Submited By</th>
                                      @if ($finance_type == 'ca')
                                      <th>Total CA</th>
                                      @elseif ($finance_type == 'pc')
                                      <th>Total PC</th>
                                      @elseif ($finance_type == 'rm')
                                      <th>Total Reimbursement</th>
                                      @endif
                                      <th>Rejected By</th>
                                      <th>Rejection Date</th>
                                      <th>Rejection Remark</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach ($reports as $report)
                                      @if ($report->project_code == $project->project_code)
                                      <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$report->finance_week}}</td>
                                        <td>{{$report->submitted_by_name}}</td>
                                        <td>Rp.{{number_format($report->total, 0, '','.')}}</td>
                                        <td>{{$report->user_name}}</td>
                                        <td>{{$report->date}}</td>
                                        <td>{{$report->remarks}}</td>
                                      </tr>
                                      @endif
                                      @endforeach
                                    </tbody>
                                  </table>
                                </div>
                                <div class="modal-footer justify-content-right">
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><b>TUTUP</b></button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script>
  $(".rejection-table").DataTable({
      responsive: true, 
      lengthChange: true, 
      autoWidth:false,
      buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
      dom: 'Bfrtip',
    });

  $("#projects-table").DataTable({
      responsive: true, 
      lengthChange: true, 
      autoWidth:false,
      buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
      dom: 'Bfrtip',
    });
</script>
@endsection