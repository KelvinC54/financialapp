@extends('layouts.main')

@section('title','createpc')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Input Petty Cash</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Petty Cash</a></li>
              <li class="breadcrumb-item active">Input</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div>

                  <div class="card-body" id="settings">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('pcreport.store') }}">
                      @csrf
                      <!--<div class="form-group row">
                        <label>Project Code</label>
                        <input id="Project Code" type="text" placeholder="Project Code" name="Project Code" value="Project Code" autocomplete="Project Code" disabled>
                        </div>-->

                      <div class="form-group row">
                        <label for="Project Code" class="col-sm-2 col-form-label">Project Code</label>
                        <div class="col-sm-10">
                          <input id="project-id" style="display: none" type="text" name="project_id" value="{{$report->finance_id}}">
                          <input id="Project Code" type="text" placeholder="Project Code" name="Project Code" value="{{$report->finance_project_code}}" autocomplete="Project Code" disabled>
                        </div>
                      </div>

                        <div class="form-group row">
                          <label for="Project Name" class="col-sm-2 col-form-label">Project Name</label>
                          <div class="col-sm-10">
                            <input id="Project Name" type="text" placeholder="Project Name" name="Project Name" value="{{$report->finance_project_name}}" autocomplete="Project Name" disabled>
                          </div>
                        </div>

                      <div class="form-group row">
                        <label for="Week CA" class="col-sm-2 col-form-label">Week CA</label>
                        <div class="col-sm-10">
                          <input id="Week CA" type="text" placeholder="Week CA" name="Week CA" value="{{$report->finance_week}}" autocomplete="Week CA" disabled>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="Date CA" class="col-sm-2 col-form-label">Date CA</label>
                        <div class="col-sm-10">
                          <input id="Date CA" type="text" placeholder="Date CA" name="Date CA" value="{{$report->created_at}}" autocomplete="Date CA" disabled>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="Submitted" class="col-sm-2 col-form-label">Submitted by</label>
                        <div class="col-sm-10">
                          <input id="Submitted" type="text" placeholder="Submitted" name="Submitted" value="{{$report->user_name}}" autocomplete="Submitted" disabled>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="Designation" class="col-sm-2 col-form-label">Designation</label>
                        <div class="col-sm-10">
                          <input id="Designation" type="text" placeholder="Designation" name="Designation" value="{{$report->user_designation}}" autocomplete="Designation" disabled>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="Total Cash Advance" class="col-sm-2 col-form-label">Total Cash Advance</label>
                        <div class="col-sm-10">
                          {{-- view CA Total --}}
                          @if ($report->ca_rcm == null && $report->ca_ahead == null && $report->ca_bod == null)
                            <input id="Total Cash Advance" type="text" placeholder="Total Cash Advance" name="Total Cash Advance"
                          value="Rp.{{number_format($report->ca_total, 0, '','.')}}" autocomplete="Total Cash Advance" disabled>

                          {{-- view CA RCM --}}
                          @elseif ($report->ca_rcm != null  && $report->ca_ahead == null && $report->ca_bod == null)
                            <input id="Total Cash Advance" type="text" placeholder="Total Cash Advance" name="Total Cash Advance"
                          value="Rp.{{number_format($report->ca_rcm, 0, '','.')}}" autocomplete="Total Cash Advance" disabled>

                          {{-- view CA A Head --}}
                          @elseif ($report->ca_rcm != null  && $report->ca_ahead != null && $report->ca_bod == null)
                            <input id="Total Cash Advance" type="text" placeholder="Total Cash Advance" name="Total Cash Advance"
                          value="Rp.{{number_format($report->ca_ahead, 0, '','.')}}" autocomplete="Total Cash Advance" disabled>

                          {{-- view CA BOD --}}
                          @elseif ($report->ca_rcm != null  && $report->ca_ahead != null && $report->ca_bod != null)
                            <input id="Total Cash Advance" type="text" placeholder="Total Cash Advance" name="Total Cash Advance"
                          value="Rp.{{number_format($report->ca_bod, 0, '','.')}}" autocomplete="Total Cash Advance" disabled>
                          @endif
                        </div>
                      </div>

                        <div class="form-group row">
                          <label for="inputSkills" class="col-sm-2 col-form-label">Total Petty Cash</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="pc_total" name="pc_total" placeholder="Masukkan Total PC (Hanya angka tanpa titik dan Rupiah)" required>
                          </div>
                        </div>

                        {{-- <div class="form-group row">
                          <label for="inputSkills" class="col-sm-2 col-form-label">Total Balance</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputSkills" placeholder="Masukkan selisih dari CA & PC (Hanya angka tanpa titik dan Rupiah)">
                          </div>
                        </div> --}}

                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Pilih Cara Upload File PC</label>
                          <div class="col-sm-10">
                            <label class="col-form-label pb-3">
                              <input class="upload_option" type="radio" name="upload_option" value="URL" checked> URL
                            </label>
                            <br>
                            <label class="col-form-label pb-3">
                              <input class="upload_option" type="radio"name="upload_option" value="FILE"> Upload File
                            </label>
                          </div>
                        </div>

                        <div class="form-group row use-file" style="display:none;">
                          <label for="inputSkills" class="col-sm-2 col-form-label">File PC</label>
                          <div class="col-sm-10">
                            <input type="file" name="pcfile[]" multiple/>
                          </div>
                        </div>

                        <div class="form-group row use-url">
                          <label for="pc_url" class="col-sm-2 col-form-label">URL File PC</label>
                          <div class="col-sm-10">
                            <input type="url" name="pc_url" id="pc_url" placeholder="www.xxx.com/xx" />
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Pilih Cara Upload Nota transfer Sisa CA</label>
                          <div class="col-sm-10">
                            <label class="col-form-label pb-3">
                              <input class="upload_option2" type="radio"name="upload_option2" value="FILE" checked> Upload File
                            </label>
                            <br>
                            <!--<label class="col-form-label pb-3">-->
                            <!--  <input class="upload_option2" type="radio" name="upload_option2" value="URL" disabled> URL-->
                            <!--</label>-->
                          </div>
                        </div>

                        <div class="form-group row use-file2" >
                          <label for="inputSkills" class="col-sm-2 col-form-label">Nota transfer Sisa CA</label>
                          <div class="col-sm-10">
                            <input type="file" name="pc_receipt_back_from_ca[]" multiple/>
                          </div>
                        </div>

                        <div class="form-group row use-url2" style="display:none;">
                          <label for="pc_receipt_back_from_ca" class="col-sm-2 col-form-label">URL Nota Transfer</label>
                          <div class="col-sm-10">
                            <input type="url" name="pc_receipt_back_from_ca" id="pc_receipt_back_from_ca" placeholder="www.xxx.com/xx" />
                          </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Remark PC From Creator</label>
                            <div class="col-sm-10">
                              <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="10" placeholder="Inputkan Catatan untuk PC yang diajukan" required></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="offset-sm-2 col-sm-10">
                            <button type="submit" class="btn btn-danger">Submit</button>
                          </div>
                        </div>
                      </form>
                      <p>Sementara ini upload bukti transfer sisa CA hanya melalui Upload File saja.</p>
                    </div>
                  </div>
                  <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script>

  $('.upload_option').on('click', function() {
          var upload_option = $('input[name="upload_option"]:checked').val();
          if (upload_option == 'URL') {
              $('.use-url').show();
              $('.use-file').hide();
          } else if (upload_option == 'FILE'){
              $('.use-url').hide();
              $('.use-file').show();
          }
      });

  $('.upload_option2').on('click', function() {
          var upload_option = $('input[name="upload_option2"]:checked').val();
          if (upload_option == 'FILE'){
              $('.use-url2').hide();
              $('.use-file2').show();
          } else if 
          (upload_option == 'URL') {
              $('.use-url2').show();
              $('.use-file2').hide();
          }
      });

</script>
@endsection
