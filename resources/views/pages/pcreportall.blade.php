@extends('layouts.main')

@section('title','pc_report')

@section('main-content')

<style type="text/css">
.step {
	display: none;
}
.step.active {
	display: block;
}
</style>
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">All Petty Cash Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Petty Cash</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <!-- /.card -->
              <div class="card">
                <div class="card-header col-md-12">
                  @if(Session::has('delete_success'))
                    <div class="alert alert-warning">
                        {{Session::get('delete_success')}}
                    </div>
                  @endif
                  @if(Session::has('reject_success'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                      {{Session::get('reject_success')}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  @endif
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-2">
                  <table id="pc-table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Date Created</th>
                      <th>Date Updated</th>
                      <th>Project Code</th>
                      <th>Project Name</th>
                      <th>Type CA</th>
                      <th>Week CA</th>
                      <th>Submited By</th>
                      <th>Total CA</th>
                      <th>Total PC</th>
                      <th>Sisa CA</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($reports as $r)
                      @if ( (Auth::user()->jobdesk == "webadmin" && $r->pc_status >= 1) ||
                        (Auth::user()->jobdesk == "3" && $r->pc_status >= 1) ||
                        (Auth::user()->jobdesk == "4" && $r->pc_status >= 1) ||
                        (Auth::user()->jobdesk == "6" && $r->pc_status >= 1) || 
                        (Auth::user()->jobdesk == "7" && $r->pc_status >= 1) || 
                        (Auth::user()->jobdesk == "8" && $r->pc_status >= 1) ||
                        (Auth::user()->jobdesk == "9" && $r->pc_status >= 1) 
                      )
                      <tr>
                          <td>{{$r->finance_id}}</td>
                          <td>{{$r->created_at}}</td>
                          <td>{{$r->updated_at}}</td>
                          <td>{{$r->finance_project_code}}</td>
                          <td>{{$r->finance_project_name}}</td>
                          <td>{{$r->ca_type}}</td>
                          <td>{{$r->finance_week}}</td>
                          <td>{{$r->user_name}}</td>
                          {{-- view CA Total --}}
                          @if ($r->ca_rcm == null && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_total, 0, '','.')}}</td>

                          {{-- view CA RCM --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_rcm, 0, '','.')}}</td>

                          {{-- view CA A Head --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_ahead, 0, '','.')}}</td>

                          {{-- view CA BOD --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod != null)
                            <td>Rp.{{number_format($r->ca_bod, 0, '','.')}}</td>
                          @endif
                          
                          <td>Rp.{{number_format($r->pc_total, 0, '','.')}}</td>
                          
                          {{-- view CA Total - PC Total --}}
                          @if ($r->ca_rcm == null && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_total - $r->pc_total, 0, '','.')}}</td>

                          {{-- view CA RCM - PC Total --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead == null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_rcm - $r->pc_total, 0, '','.')}}</td>

                          {{-- view CA A Head - PC Total --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod == null)
                            <td>Rp.{{number_format($r->ca_ahead - $r->pc_total, 0, '','.')}}</td>

                          {{-- view CA BOD - PC Total --}}
                          @elseif ($r->ca_rcm != null  && $r->ca_ahead != null && $r->ca_bod != null)
                            <td>Rp.{{number_format($r->ca_bod - $r->pc_total, 0, '','.')}}</td>
                          @endif
                        
                          <td><?php
                            switch ($r->pc_status) {
                                case '1':
                                    echo "Belum di Review";
                                    break;
                                case '9':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by Akuntan Transfer";
                                    break;
                                default:

                                    break;
                            }
                            ?>
                          </td>
                        
                      </tr>
                      @endif
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                          <th></th>
                          <th>Date Created</th>
                          <th>Project Code</th>
                          <th>Project Name</th>
                          <th>Type CA</th>
                          <th>Week CA</th>
                          <th>Submitted By</th>
                          <th>Total CA</th>
                          <th>Total PC</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script>
  var table = $("#pc-table").DataTable({
    // responsive: true,
    order: [[0, 'desc']],
    lengthChange: false, 
    autoWidth:false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    dom: 'Bfrtip',
  });

  $('#pc-table tfoot th').each( function (i) {
      var title = $('#pc-table thead th').eq( $(this).index() ).text();
      $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" data-index="'+i+'" />' );
  } );

  $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
    table
          .column( $(this).data('index') )
          .search( this.value )
          .draw();
  } );
</script>
@endsection
