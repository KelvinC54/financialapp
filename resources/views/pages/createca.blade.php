@extends('layouts.main')

@section('title','createca')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

#myInput {
  box-sizing: border-box;
  background-image: url('/img/searchicon.png');
  background-position: 10px 5px;
  background-size: 5%;
  background-repeat: no-repeat;
  font-size: 16px;
  padding: 14px 20px 12px 45px;
  border: none;
  border-bottom: 1px solid #ddd;
}

#myInput:focus {outline: 3px solid #ddd;}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #F7F8F9;
  min-width: 230px;
  max-height: 300px;
  overflow: scroll;
  border: 1px solid #ddd;
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Input Cash Advance</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Cash Advance</a></li>
              <li class="breadcrumb-item active">Input</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">

              @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
              <!-- /.card-header -->

            <!-- /.card -->

              <!-- <div class="card">
                <div class="card-header col-md-12">
                  <button col-md-12 type="button" class="btn btn-outline-primary">Add New CA</button></li>
                </div>-->
              <!-- /.card-header -->
              <div>
              <div class="card-body" id="settings">
                <form id="form-ca" class="form-horizontal" action="{{ route('careport.store') }}" method="POST" enctype="multipart/form-data">
                  @csrf

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Code</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control " id="finance_project_code" name="finance_project_code" placeholder="">
                        <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="right" onclick="myFunction()"  title="Opsi"> 
                        <i class="fas fa-chevron-circle-down"></i></button>
                        <div id="myDropdown" class="dropdown-content">
                          <input type="text" class="form-control" placeholder="Search.." id="myInput" onkeyup="filterFunction()">
                          @foreach ($projects as $p)
                            <a href="#{{$p->project_code}}" data-value-code="{{$p->project_code}}" data-value-name="{{$p->project_name}}">
                              {{$p->project_code}} &nbsp; - &nbsp; {{$p->project_name}}
                            </a>
                          @endforeach
                    </div>
                   </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Project Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="finance_project_name" name="finance_project_name" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Type CA</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="ca_type" name="ca_type" placeholder="OPS, BBM, etc..">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Week CA</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="finance_week" name="finance_week" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_id" class="col-sm-2 col-form-label">Submitted by NIP | Name</label>
                    <div class="col-sm-10">
                        <input id="user_id" type="text" name="user_id" value="{{ Auth::user()->nip }}" disabled>
                        <input id="user_name" type="text" name="user_name" value="{{ Auth::user()->name }}" disabled>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="user_designation"class="col-sm-2 col-form-label">Designation</label>
                    <div class="col-sm-10">
                      <input id="user_designation" type="text" name="user_designation" value="{{ Auth::user()->designation }}" disabled>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Total Cash Advance</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="ca_total" name="ca_total" placeholder="ex: 1000000 (Hanya angka tanpa titik dan Rupiah)">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pilih Cara Upload File CA</label>
                    <div class="col-sm-10">
                      <label class="col-form-label pb-3">
                        <input class="upload_option" type="radio" name="upload_option" value="URL" checked> URL
                      </label>
                      <br>
                      <label class="col-form-label pb-3">
                        <input class="upload_option" type="radio"name="upload_option" value="FILE"> Upload File
                      </label>
                    </div>
                  </div>

                  <div class="form-group row use-file" style="display:none;">
                    <label class="col-sm-2 col-form-label">File CA</label>
                    <div class="col-sm-10">
                        <input type="file" id="cafile" name="cafile" data-value="">
                    </div>
                  </div>

                  <div class="form-group row use-url">
                    <label class="col-sm-2 col-form-label">URL File CA</label>
                    <div class="col-sm-10">
                      <input id="ca_url" type="text" name="ca_url" placeholder="www.url.com/subdomain" />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Remark CA From Creator</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="10" placeholder="Inputkan Catatan untuk CA yang diajukan" required></textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- Sweet ALert -->
<link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Sweet Alert -->
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Ajax -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>
  function uploadFileLive(input_type) {
      // declare FormData dari semua input
      var form = document.getElementById('form-ca');
      var formData = new FormData(form);

      // declare FormData kosong
      var formDataUpload = new FormData();

      if (input_type == 'pdf'){
          var file_yang_diupload = document.getElementById('cafile').files[0];
      }else {
          var file_yang_diupload = formData.get('cafile');
      }

      formDataUpload.append('cafile', file_yang_diupload);
      formDataUpload.append('finance_project_code', formData.get('finance_project_code'));

      var url_upload = "{{URL('/api/careport/upload')}}";

      //AJAX request
      $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
      });
      $.ajax({
          url: url_upload,
          method: "POST",
          data: formDataUpload,
          contentType: false,
          cache: false,
          processData: false,
          dataType: "json",
          success:function(data){
              Swal.fire({
                    icon:'success',
                    title: 'File Berhasil Diupload!',
                    text: 'Ukuran dan ekstensi file sudah tepat, dan sudah disimpan',
                    showConfirmButton: true,
              });
              $('#cafile').attr('data-value', data.nama_file);
          },
          error: function(data){
              Swal.fire({
                      icon: 'error',
                      title: 'Upload Gagal!',
                      text: 'Terdapat kesalahan sistem, mohon coba lagi.',
              });
          }
      });
  }

    $("input[type='file']").on("change", function() {
        if (this.files[0].size > 10000000) {
          Swal.fire({
                      icon: 'error',
                      title: 'Ukuran File Terlalu Besar',
                      text: 'Silakan upload file dengan ukuran kurang dari 10MB',
              });
          this.value = '';
          return false;
        } else {
          var pathFile = this.value;
          var ekstensiOk = /(\.jpg|\.jpeg|\.png|\.doc|\.docx|\.xlsx|\.xls|\.txt|\.pdf|\.zip|\.csv)$/i;
          if(!ekstensiOk.exec(pathFile)){
              Swal.fire({
                      icon: 'error',
                      title: 'Ekstensi File Salah!',
                      text: 'Silakan upload file dengan ekstensi jpg,jpeg,png,doc,docx,xlsx,xls,txt,pdf,zip',
              });
              this.value = '';
              return false;
          }else{
            // create CA tanpa ajax
            Swal.fire({
                    icon:'success',
                    title: 'File Benar!',
                    text: 'Ukuran dan ekstensi file sudah tepat',
                    showConfirmButton: true,
              });

            // create CA with ajax
            // var ekstensiPdf = /(\.pdf)$/i;
            // if (ekstensiOk.exec(pathFile)) {
            //   uploadFileLive('pdf');
            // } else {
            //   uploadFileLive('others');
            // }
          }
        }
    });

    $('.upload_option').on('click', function() {
        var upload_option = $('input[name="upload_option"]:checked').val();
        if (upload_option == 'URL') {
            $('.use-url').show();
            $('.use-file').hide();
        } else if (upload_option == 'FILE'){
            $('.use-url').hide();
            $('.use-file').show();
        }
    });

    $('.listSearch li').each(function(){
            $(this).attr('searchData', $(this).text().toLowerCase());
        });

        $('.boxSearch').on('keyup', function(){
        var dataList = $(this).val().toLowerCase();
            $('.listSearch li').each(function(){
                if ($(this).filter('[searchData *= ' + dataList + ']').length > 0 || dataList.length < 1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });

    function myFunction() {
      document.getElementById("myDropdown").classList.toggle("show");
    }

    function filterFunction() {
      var input, filter, ul, li, a, i;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      div = document.getElementById("myDropdown");
      a = div.getElementsByTagName("a");
      for (i = 0; i < a.length; i++) {
        txtValue = a[i].textContent || a[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          a[i].style.display = "";
        } else {
          a[i].style.display = "none";
        }
      }
    }

    $('#myDropdown a').on('click', function() {
      $('#finance_project_code').val($(this).attr('data-value-code'));
      $('#finance_project_name').val($(this).attr('data-value-name'));
    });

</script>
@endsection
