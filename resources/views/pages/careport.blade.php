@extends('layouts.main')

@section('title','careport')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

.clickable {
    cursor: pointer;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Cash Advance Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Cash Advance</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->

            <!-- /.card -->

            <div class="card">
              <div class="card-header col-md-12">
                @if(Session::has('delete_success'))
                <div class="alert alert-warning">
                    {{Session::get('delete_success')}}
                </div>
                @endif
                @if(Session::has('reject_success'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  {{Session::get('reject_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                {{-- @if(Session::has('allow_success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{Session::get('allow_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if(Session::has('revoke_success'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  {{Session::get('revoke_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif --}}
                <a href="{{ route('careport.create') }}">
                <button col-md-12 type="button" class="btn btn-outline-primary">Add New CA</button></a>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-2">
                <table id="ca-table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Date Created</th>
                    <th>Project Code</th>
                    <th>Project Name</th>
                    <th>Type CA</th>
                    <th>Week CA</th>
                    <th>Submited By</th>
                    <th>Total CA</th>
                    <th>CA Approved RCM</th>
                    <th>CA Approved Ak. Head</th>
                    <th>CA Approved BOD</th>
                    <th>Last Status</th>
                    <th>Current Status</th>
                    <th>Aging Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($reports as $r)
                  @if (
                      (Auth::user()->jobdesk == "webadmin" && $r->ca_status >= 0) ||
                      
                      {{-- USER --}}
                      ((Auth::user()->jobdesk == "1" && $r->user_id == Auth::user()->id) && $r->ca_status < 10) ||
                      
                      {{-- PM REGIONAL --}}
                      ((Auth::user()->jobdesk == "2" && $r->ca_status == 0) || (Auth::user()->jobdesk == "2" && $r->user_id == Auth::user()->id)) ||
                      
                      {{-- PM NASIONAL --}}
                      ((Auth::user()->jobdesk == "3" && $r->ca_status == 2) || (Auth::user()->jobdesk == "3" && $r->user_id == Auth::user()->id)) ||
                      
                      {{-- RCM --}}
                      ((Auth::user()->jobdesk == "4" && $r->ca_status == 3) || (Auth::user()->jobdesk == "4" && $r->user_id == Auth::user()->id)) ||
                      
                      {{-- DH --}}
                      ((Auth::user()->jobdesk == "5" && $r->ca_status == 4) || (Auth::user()->jobdesk == "5" && $r->user_id == Auth::user()->id)) ||
                      
                      {{-- AK. APPRV --}}
                      ((Auth::user()->jobdesk == "6" && $r->ca_status == 5) || (Auth::user()->jobdesk == "6" && $r->user_id == Auth::user()->id)) ||
                      
                      {{-- AK. HEAD --}}
                      ((Auth::user()->jobdesk == "7" && $r->ca_status == 6) || (Auth::user()->jobdesk == "7" && $r->user_id == Auth::user()->id)) ||
                      
                      {{-- BOD --}}
                      ((Auth::user()->jobdesk == "8" && $r->ca_status == 7) || (Auth::user()->jobdesk == "8" && $r->user_id == Auth::user()->id)) ||
                      
                      {{-- AK. TRANSFER --}}
                      ((Auth::user()->jobdesk == "9" && $r->ca_status == 8) || (Auth::user()->jobdesk == "9" && $r->user_id == Auth::user()->id && $r->ca_status != 10))
                      )
                    <tr>
                      <td>{{$r->finance_id}}</td>
                      <td>{{$r->created_at}}</td>
                      <td>{{$r->finance_project_code}}</td>
                      <td>{{$r->finance_project_name}}</td>
                      <td>{{$r->ca_type}}</td>
                      <td>{{$r->finance_week}}</td>
                      <td>{{$r->user_name}}</td>

                      <td>Rp.{{number_format($r->ca_total, 0, '','.')}}</td>
                      <td>Rp.{{number_format($r->ca_rcm, 0, '','.')}}</td>
                      <td>Rp.{{number_format($r->ca_ahead, 0, '','.')}}</td>
                      <td>Rp.{{number_format($r->ca_bod, 0, '','.')}}</td>

                      @if($r->ca_total >= 10000000)
                      
                            {{-- LAST STATUS --}}
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by BOD";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                case '10':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                            </td>
                            
                            {{-- CURRENT STATUS --}}
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "waiting PM Regional Approval";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "waiting PM National Approval";
                                    break;
                                case '3':
                                    echo "waiting RCM Approval";
                                    break;
                                case '4':
                                    echo "waiting DH Approval";
                                    break;
                                case '5':
                                    echo "waiting Ak. App. Approval";
                                    break;
                                case '6':
                                    echo "waiting Ak. Head Approval";
                                    break;
                                case '7':
                                    echo "waiting BOD Approval";
                                    break;
                                case '8':
                                    echo "waiting Ak. Transfer Approval";
                                    break;
                                default:
                                    echo "-";
                                    break;
                              }
                            ?>
                            </td>
                            
                      @else
                            {{-- LAST STATUS --}}
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                case '10':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                            </td>
                            
                            {{-- CURRENT STATUS --}}
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "waiting PM Regional Approval";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "waiting PM National Approval";
                                    break;
                                case '3':
                                    echo "waiting RCM Approval";
                                    break;
                                case '4':
                                    echo "waiting DH Approval";
                                    break;
                                case '5':
                                    echo "waiting Ak. App. Approval";
                                    break;
                                case '6':
                                    echo "waiting Ak. Head Approval";
                                    break;
                                case '8':
                                    echo "waiting Ak. Transfer Approval";
                                    break;
                                default:
                                    echo "-";
                                    break;
                              }
                            ?>
                            </td>
                            
                      @endif
                      
                      <td>
                        <?php
                          if (isset($r->updated_at)) {
                            $diff = date_diff(date_create($r->updated_at, timezone_open("Asia/Jakarta")), $current_date_time);
                            echo $diff->d;
                          } else {
                            echo "-";
                          }

                        ?>
                        Day(s)
                      </td>
                      <td>
                        <!-- Call to action buttons -->
                        <ul class="list-inline m-0">

                          {{-- button VIEW USER REMARK --}}
                          <li class="list-inline-item">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#user_remarks_{{ $r->finance_id }}">Notes CA</button>
                          </li>
                          <div id="user_remarks_{{ $r->finance_id }}" class="modal fade">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Remarks from {{ $r->user_name }}:</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>{{ $r->user_remarks }}</p>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                    </div>
                                </div>
                            </div>
                          </div>
                          
                          {{-- button VIEW URL / FILE --}}
                          <li class="list-inline-item">
                            @if ($r->ca_file == null)
                              <a target="_blank" href="{{url('//' . $r->ca_url) }}">
                                <button type="button" class="btn btn-warning btn-sm">View CA</button>
                              </a>
                            @else
                              <a target="_blank" href="{{ asset('cafile/' . $r->ca_file) }}">
                                <button type="button" class="btn btn-info btn-sm">Download CA</button>
                              </a>
                            @endif
                          </li>

                          {{-- button INPUT PC saat status == 8 / CA Transfered --}}
                          @if ($r->ca_status == 9)
                            <li class="list-inline-item">
                              <a href="{{ url('pcreport/'. $r->finance_id .'/edit') }}">
                              <button type="button" class="btn btn-warning btn-sm">Input PC</button>
                              </a>
                            </li>
                          @endif

                          {{-- button REMARKS CA --}}
                          @if ($r->ca_status == 1 || $r->ca_status == 0)
                            <li class="list-inline-item">
                              <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#remarks_{{ $r->finance_id }}">Remarks</button>
                            </li>

                            <div id="remarks_{{ $r->finance_id }}" class="modal fade">
                              <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h4 class="modal-title">CA Remarks:</h4>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                      <div class="modal-body">
                                          <p>{{ $r->ca_remarks }}</p>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          @endif

                          {{-- button EDIT, button DELETE --}}
                          @if ((($r->ca_status == 0 || $r->ca_status == 1) && (Auth::user()->jobdesk == "1" || Auth::user()->jobdesk == "3")) || Auth::user()->jobdesk == "webadmin")

                            {{-- button EDIT --}}
                            <li class="list-inline-item">
                              <a href="{{ url('careport/'. $r ->finance_id .'/edit') }}">
                              <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit CA"><i class="fa fa-edit"></i></button>
                              </a>
                            </li>

                            {{-- button DELETE --}}
                            <li class="list-inline-item">
                            <form action="careport/{{ $r->finance_id }}" method="POST">
                              <input type="hidden" name="_method" value="delete">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="btn btn-danger btn-sm rounded-0" type="submit" data-toggle="tooltip" data-placement="top" title="Delete CA"><i class="fa fa-trash"></i></button>
                            </form>
                            </li>

                          @endif

                          {{-- button ACCEPT, button REJECT --}}

                          {{-- PM Reg approve & reject --}}
                          @if (Auth::user()->jobdesk == "2" && $r->ca_status == 0)
                            <li class="list-inline-item">
                              <a href="{{ url('careport/approve-0/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pm-reg-{{ $r->finance_id }}" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-pm-reg-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('careport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject CA</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- PM National approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "3" && $r->ca_status == 2)
                            <li class="list-inline-item">
                              <a href="{{ url('careport/approve-2/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-pm-nat-{{ $r->finance_id }}" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-pm-nat-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('careport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject CA</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- RCM approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "4" && $r->ca_status == 3)
                            <li class="list-inline-item">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="modal" data-target="#approve_rcm_{{ $r->finance_id }}" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>

                                {{-- Modal Approval RCM --}}
                                <div id="approve_rcm_{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Approve?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('careport/approve-3/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="ca_ahead" class="col-form-label">Nominal CA Total Yang di Approve :</label>
                                            <input class="form-control" id="ca_rcm" name="ca_rcm" rows="1" placeholder="Hanya Inputkan Angka!" required>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-success">Approve</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-rcm-{{ $r->finance_id }}" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-rcm-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('careport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject CA</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- DH Approval approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "5" && $r->ca_status == 4)
                            <li class="list-inline-item">
                              <a href="{{ url('careport/approve-4/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-dh-{{ $r->finance_id }}" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-dh-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('careport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject CA</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- Akuntan Approval approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "6" && $r->ca_status == 5)
                            <li class="list-inline-item">
                              <a href="{{ url('careport/approve-5/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-{{ $r->finance_id }}" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-akun-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('careport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject CA</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>

                          {{-- Akuntan Head approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "7" && $r->ca_status == 6)
                            <li class="list-inline-item">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="modal" data-target="#approve_a_head_{{ $r->finance_id }}" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                                {{-- Modal Approval Akuntan Head --}}
                                <div id="approve_a_head_{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Approve?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('careport/approve-6/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="ca_ahead" class="col-form-label">Nominal CA Total Yang di Approve :</label>
                                            <input class="form-control" id="ca_ahead" name="ca_ahead" rows="1" placeholder="Hanya Inputkan Angka!" required>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-success">Approve</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>
                            <li class="list-inline-item">
                              <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-head-{{ $r->finance_id }}" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>
                              
                              <div id="reject-akun-head-{{ $r->finance_id }}" class="modal fade">
                                <div class="modal-dialog modal-dialog-scrollable">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form action="{{ url('careport/reject/' . $r->finance_id) }}" method="GET">
                                        <div class="form-group">
                                          <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                          <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="button btn btn-danger">Reject CA</a>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                            {{-- <li class="list-inline-item">
                              @if ($r->multiple_ca_on_project == 0)
                                <a href="{{ url('/careport/multi/ca/allow/' . $r->finance_id) }}" data-value-project="{{$r->finance_project_code}}" data-value-user="{{$r->user_name}}" class="allow-multiple">
                                  <button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Allow Multiple CA"><i class="fas fa-folder-plus"></i> &nbsp; Multiple CA</button>
                                </a>
                              @else
                                <a href="{{ url('/careport/multi/ca/revoke/' . $r->finance_id) }}" data-value-project="{{$r->finance_project_code}}" data-value-user="{{$r->user_name}}" class="revoke-multiple">
                                  <button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Revoke Multiple CA Permission"><i class="fas fa-folder-minus"></i> &nbsp; Revoke</button>
                                </a>
                              @endif
                            </li> --}}

                          {{--BOD approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "8" && $r->ca_status == 7)
                            <!--<li class="list-inline-item">-->
                            <!--    <a href="{{ url('careport/approve-7/' . $r->finance_id) }}">-->
                            <!--        <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top-section" title="Acc CA"><i class="fa fa-check"></i></button>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <li class="list-inline-item">
                                <button class="clickable btn btn-success btn-sm rounded-0 " type="button" data-toggle="modal" data-target="#approve_bod_{{ $r->finance_id }}" data-placement="top-section" title="Acc CA"><i class="fas fa-check"></i></button>
                                {{-- Modal Approval BOD --}}
                                <div id="approve_bod_{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Approve?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body clickable">
                                        <form action="{{ url('careport/approve-7/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="ca_ahead" class="col-form-label">Nominal CA Total Yang di Approve :</label>
                                            <input class="form-control" id="ca_bod" name="ca_bod" rows="1" placeholder="Hanya Inputkan Angka!" required>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-success">Approve</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>
                            <li class="list-inline-item">
                                <button class="clickable btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-bod-{{ $r->finance_id }}" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>

                                <div id="reject-bod-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('careport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject CA</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>

                          {{-- Akuntan Transfer approve & reject --}}
                          @elseif (Auth::user()->jobdesk == "9" && $r->ca_status == 8)
                            <li class="list-inline-item">
                              <a href="{{ url('careport/approve-8/' . $r->finance_id) }}">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Acc CA"><i class="fas fa-check"></i></button>
                              </a>
                            </li>
                            <li class="list-inline-item">
                                <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="modal" data-target="#reject-akun-tf-{{ $r->finance_id }}" data-placement="top" title="Reject CA"><i class="fas fa-window-close"></i></button>

                                <div id="reject-akun-tf-{{ $r->finance_id }}" class="modal fade">
                                  <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('careport/reject/' . $r->finance_id) }}" method="GET">
                                          <div class="form-group">
                                            <label for="doc_remarks" class="col-form-label">Alasan Reject:</label>
                                            <textarea class="form-control" id="doc_remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                          </div>
                                      </div>
                                      <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="button btn btn-danger">Reject CA</a>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </li>
                          @endif
                        </ul>
                      </td>
                    </tr>
                  @endif
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                      <th></th>
                      <th>Date Created</th>
                      <th>Project Code</th>
                      <th>Project Name</th>
                      <th>Week CA</th>
                      <th>Submitted By</th>
                      <th>Total CA</th>
                  </tr>
                </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->
<!-- REQUIRED SCRIPTS -->
<!-- Sweet ALert -->
<link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Sweet Alert -->
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
  $(document).ready(function() {
    var table = $("#ca-table").DataTable({
      // responsive: true, 
      order: [[0, 'desc']],
      lengthChange: false, 
      autoWidth:false,
      buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
      dom: 'Bfrtip',
    });

    $('.allow-multiple').click(function(e) {
        e.preventDefault();
        const HREF = $(this).attr('href');
        const USER_NAME = $(this).attr('data-value-user');
        const PROJECT_CODE = $(this).attr('data-value-project');
        Swal.fire({
                icon: 'warning',
                title: 'Apakah Anda yakin?',
                text: 'Dengan menekan YA, maka user ('+ USER_NAME + ') akan dapat memasukkan CA lain untuk project (' + PROJECT_CODE + ')',
                showCancelButton: true,
                confirmButtonText: '  YA  ',
                cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                document.location.href = HREF;
            }
        });
        });

    $('.revoke-multiple').click(function(e) {
        e.preventDefault();
        const HREF = $(this).attr('href');
        const USER_NAME = $(this).attr('data-value-user');
        const PROJECT_CODE = $(this).attr('data-value-project');
        Swal.fire({
                icon: 'warning',
                title: 'Apakah Anda yakin?',
                text: 'Dengan menekan YA, maka user ('+ USER_NAME + ') tidak akan dapat memasukkan CA lain untuk project (' + PROJECT_CODE + ')',
                showCancelButton: true,
                confirmButtonText: '  YA  ',
                cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                document.location.href = HREF;
            }
        });
        });

    $('#ca-table tfoot th').each( function (i) {
        var title = $('#ca-table thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" data-index="'+i+'" />' );
    } );
  
    $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
      table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );
    
  });

</script>
@endsection
