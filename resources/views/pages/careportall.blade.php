@extends('layouts.main')

@section('title','careport')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">All Cash Advance Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">All Cash Advance</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->

            <!-- /.card -->

            <div class="card">
              <div class="card-header col-md-12">
                @if(Session::has('delete_success'))
                <div class="alert alert-warning">
                    {{Session::get('delete_success')}}
                </div>
                @endif
                @if(Session::has('reject_success'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  {{Session::get('reject_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                {{-- @if(Session::has('allow_success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{Session::get('allow_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if(Session::has('revoke_success'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  {{Session::get('revoke_success')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif --}}
                <!--<a href="{{ route('careport.create') }}">-->
                <!--<button col-md-12 type="button" class="btn btn-outline-primary">Add New CA</button></a>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-2">
                <table id="ca-table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Date Created</th>
                    <th>Project Code</th>
                    <th>Project Name</th>
                    <th>Type CA</th>
                    <th>Week CA</th>
                    <th>Submited By</th>
                    <th>Total CA</th>
                    <th>CA Approved RCM</th>
                    <th>CA Approved Ak. Head</th>
                    <th>CA Approved BOD</th>
                    <th>Last Status</th>
                    <th>Current Status</th>
                    <th>Aging Date</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($reports as $r)
                  @if ( (Auth::user()->jobdesk == "webadmin" && $r->ca_status >= 0) ||
                        (Auth::user()->jobdesk == "3" && $r->ca_status >= 0) ||
                        (Auth::user()->jobdesk == "4" && $r->ca_status >= 0) ||
                        (Auth::user()->jobdesk == "6" && $r->ca_status >= 0) || 
                        (Auth::user()->jobdesk == "7" && $r->ca_status >= 0) || 
                        (Auth::user()->jobdesk == "8" && $r->ca_status >= 0) ||
                        (Auth::user()->jobdesk == "9" && $r->ca_status >= 0) 
                      )
                    <tr>
                      <td>{{$r->finance_id}}</td>
                      <td>{{$r->created_at}}</td>
                      <td>{{$r->finance_project_code}}</td>
                      <td>{{$r->finance_project_name}}</td>
                      <td>{{$r->ca_type}}</td>
                      <td>{{$r->finance_week}}</td>
                      <td>{{$r->user_name}}</td>

                      <td>Rp.{{number_format($r->ca_total, 0, '','.')}}</td>
                      <td>Rp.{{number_format($r->ca_rcm, 0, '','.')}}</td>
                      <td>Rp.{{number_format($r->ca_ahead, 0, '','.')}}</td>
                      <td>Rp.{{number_format($r->ca_bod, 0, '','.')}}</td>

                      @if($r->ca_total >= 10000000)
                      
                            {{-- LAST STATUS --}}
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by BOD";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                case '10':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                            </td>
                            
                            {{-- CURRENT STATUS --}}
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "waiting PM Regional Approval";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "waiting PM National Approval";
                                    break;
                                case '3':
                                    echo "waiting RCM Approval";
                                    break;
                                case '4':
                                    echo "waiting DH Approval";
                                    break;
                                case '5':
                                    echo "waiting Ak. App. Approval";
                                    break;
                                case '6':
                                    echo "waiting Ak. Head Approval";
                                    break;
                                case '7':
                                    echo "waiting BOD Approval";
                                    break;
                                case '8':
                                    echo "waiting Ak. Transfer Approval";
                                    break;
                                default:
                                    echo "-";
                                    break;
                              }
                            ?>
                            </td>
                            
                      @else
                            {{-- LAST STATUS --}}
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "Belum di Review";
                                    break;
                                case '2':
                                    echo "Approved by PM Regional";
                                    break;
                                case '3':
                                    echo "Approved by PM National";
                                    break;
                                case '4':
                                    echo "Approved by RCM";
                                    break;
                                case '5':
                                    echo "Approved by DH";
                                    break;
                                case '6':
                                    echo "Approved by Akuntan Approval";
                                    break;
                                case '7':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '8':
                                    echo "Approved by Akuntan Head";
                                    break;
                                case '9':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                case '10':
                                    echo "Transfered by Akuntan Transfer";
                                    break;
                                default:
                                    break;
                              }
                            ?>
                            </td>
                            
                            {{-- CURRENT STATUS --}}
                            <td>
                            <?php
                              switch ($r->ca_status) {
                                case '0':
                                    echo "waiting PM Regional Approval";
                                    break;
                                case '1':
                                    echo "Rejected";
                                    break;
                                case '2':
                                    echo "waiting PM National Approval";
                                    break;
                                case '3':
                                    echo "waiting RCM Approval";
                                    break;
                                case '4':
                                    echo "waiting DH Approval";
                                    break;
                                case '5':
                                    echo "waiting Ak. App. Approval";
                                    break;
                                case '6':
                                    echo "waiting Ak. Head Approval";
                                    break;
                                case '8':
                                    echo "waiting Ak. Transfer Approval";
                                    break;
                                default:
                                    echo "-";
                                    break;
                              }
                            ?>
                            </td>
                            
                      @endif
                      
                      <td>
                        <?php
                          if (isset($r->updated_at)) {
                            $diff = date_diff(date_create($r->updated_at, timezone_open("Asia/Jakarta")), $current_date_time);
                            echo $diff->d;
                          } else {
                            echo "-";
                          }

                        ?>
                        Day(s)
                      </td>
                    </tr>
                  @endif
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                      <th></th>
                      <th>Date Created</th>
                      <th>Project Code</th>
                      <th>Project Name</th>
                      <th>Week CA</th>
                      <th>Submitted By</th>
                      <th>Total CA</th>
                  </tr>
                </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->
<!-- REQUIRED SCRIPTS -->
<!-- Sweet ALert -->
<link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Sweet Alert -->
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
  $(document).ready(function() {
    var table = $("#ca-table").DataTable({
      // responsive: true, 
      order: [[0, 'desc']],
      lengthChange: false, 
      autoWidth:false,
      buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
      dom: 'Bfrtip',
    });

    $('.allow-multiple').click(function(e) {
        e.preventDefault();
        const HREF = $(this).attr('href');
        const USER_NAME = $(this).attr('data-value-user');
        const PROJECT_CODE = $(this).attr('data-value-project');
        Swal.fire({
                icon: 'warning',
                title: 'Apakah Anda yakin?',
                text: 'Dengan menekan YA, maka user ('+ USER_NAME + ') akan dapat memasukkan CA lain untuk project (' + PROJECT_CODE + ')',
                showCancelButton: true,
                confirmButtonText: '  YA  ',
                cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                document.location.href = HREF;
            }
        });
        });

    $('.revoke-multiple').click(function(e) {
        e.preventDefault();
        const HREF = $(this).attr('href');
        const USER_NAME = $(this).attr('data-value-user');
        const PROJECT_CODE = $(this).attr('data-value-project');
        Swal.fire({
                icon: 'warning',
                title: 'Apakah Anda yakin?',
                text: 'Dengan menekan YA, maka user ('+ USER_NAME + ') tidak akan dapat memasukkan CA lain untuk project (' + PROJECT_CODE + ')',
                showCancelButton: true,
                confirmButtonText: '  YA  ',
                cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                document.location.href = HREF;
            }
        });
        });

    $('#ca-table tfoot th').each( function (i) {
        var title = $('#ca-table thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" data-index="'+i+'" />' );
    } );
  
    $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
      table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );
    
  });

</script>
@endsection
